/** @module domTools */

//==================================================
// OUTILS DE MANIPULATION DU DOM
//==================================================

/**
 * Crée un élément HTML 
 * 
 * @param {string} element 
 * @returns {object} l'élément HTML créé
 * 
 * @author Sébastien LOZANO
 */
 export function createNode(element) {
    return document.createElement(element);
}

/**
 * Ajoute un nœud à la fin de la liste des enfants d'un nœud parent spécifié.
 * @param {object} parent le noeud parent
 * @param {object} el l'élément enfant à ajouter
 * @returns {object} l'enfant ajouté au noeud parent
 */

export function append(parent, el) {
  return parent.appendChild(el);
}

/**
 * Une fonction pour récupérer toutes les balises du DOM dont l'id contient un certain pattern
 * 
 * @param {regex} regex - une regex
 * @returns {Array} output - un tableau avec toutes les id satisfaisant au pattern de la regex
 * 
 * @author Sébastien LOZANO
 */
 export function DOMRegex(regex) {
  let output = [];
  for (let i of document.querySelectorAll('*')) {
      if (regex.test(i.id)) { // id ou n'importe quel autre attribut
          output.push(i);
      }
  }
  return output;
}

/**
 * Une fonction pour montrer/cacher un element du DOM
 * 
 * @param {object} id - le querySelector de l'element à toggle
 * 
 * @author Sébastien LOZANO * 
 */

export function toggleId(idTarget) {    
  idTarget.style.display = idTarget.style.display === "none" ? idTarget.style.display = "block" : idTarget.style.display = "none";
};