/** @module profil */

/**
 * Class pour la gestion des profils
 * 
 * @author Sébastien LOZANO
 */

class Profil {
    /**
     * Crée un profil.
     * @param {string} login - le login
     * @param {string} avatar - un string contenant l'url de l'avatar   
     * @param {string} nom - le nom
     * @param {string} prenom - le prenom
     * @param {string} parties - le tableau des parties
     * @param {string} statistiques - le tableau des statistiques 
     */
    constructor(login,avatar,nom,prenom,parties,statistiques) {
        this.login = login
        this.avatar = avatar;
        this.nom = nom;
        this.prenom = prenom;        
        this.parties = parties;      
        this.statistiques = statistiques;
        this.msgAfterConnexion = "<p>Tu accèdes aux fonctionnalités via les sections ci-dessous. Tu peux aussi consulter la documentation via le bouton dédié dans la barre de navigation.</p>"
    }

    /**
     * Une fonction qui affiche un message de bienvenue ou de transition suivi du prénom et du nom
     * @param {string} firstWord - Le mot qui débute la phrase
     * @returns 
     */
    bienvenue(firstWord) {
        return `<h3>${firstWord} ${this.prenom} ${this.nom},</h3>`;
    };

    /**
     * Pour mettre à jour le tableau des statistiques
     * 
     * @param {string} url - url du script php
     * @param {object} datas - un objet avec les éventuelles données à transmettre au script php
     * @param {string} strZone - un string avec la zone ad hoc
     * 
     */
    fetchMajStatistiques(url,datas,strZone) {
    console.log(`fecth Maj ${strZone}`); // Pour mon info dans la console            
    fetch(url, {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(datas),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((datas) => {
            // On met à jour les stats            
            this.statistiques = datas.statistiques;
            // On met à jour la zone sessionOKProfil
            document.querySelector('#sessionOKProfil').style.display = 'block'; // Au cas où si elle avait était cachée
            document.querySelector('#sessionOKProfil').innerHTML = `
            <h3>Tableau des statistiques des utilisateurs inscrits.</h3>
            <p>
            Pour chaque joueur :
            <ul>
            <li>La première ligne indique le nombre de parties de chaque catégorie.</li>
            <li>La seconde ligne indique le pourcentage de parties de chaque catégorie.</li>
            </ul>
            </p>
            <hr>
            ${this.statistiques}
            `;
            // On modifie aussi le profil stocké sur le client            
            sessionStorage.setItem('currentProfil',JSON.stringify(this));
        })
        .catch((datas) => {
            console.error("Error : ",datas.message)
        });
};
    
}

export { Profil }