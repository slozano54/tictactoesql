/** @module game */

//=======================================================
// GESTION DE LA GRID DE JEU
//=======================================================


/**
 * Class pour la gestion des grids de jeu
 * Etat courant du jeu :
 * * On utilise une chaine de neuf caractères
 * * _ correspond à une case vide
 * * X à une case prise par le joueur X
 * * O à une case prise par le joueur O
 * 
 * @author Sébastien LOZANO
 */
class Game {
    /**
     * Crée une grid de jeu
     * @param {string} currentGameState - L'état courant de la partie
     * @param {boolean} isActiveGame - Booleén indiquant si lapartie est en cours ou non
     * @param {string} activePlayer - le symbole du joueur actif
     * @param {string} logPlayer - le joueur connecté
     * @param {stirng} playerX - le login du joueur X
     * @param {stirng} nomX - le nom du joueur X
     * @param {stirng} prenomX - le prenom du joueur X
     * @param {stirng} playerO - le login du joueur O
     * @param {stirng} nomO - le nom du joueur O
     * @param {stirng} prenomO - le prenom du joueur O
     * @param {string} winner - le login du vainqueur
     * @param {string} idGame - le nom de la partie
    */
    constructor(currentGameState = "_________" , isActiveGame = true, activePlayer = 'X', logPlayer, playerX, nomX, prenomX, playerO, nomO, prenomO, winner = null , idGame) {
        this.currentGameState = currentGameState;
        this.isActiveGame = isActiveGame;
        this.activePlayer = activePlayer;
        this.logPlayer = logPlayer;
        this.playerX = playerX;
        this.nomX = nomX;
        this.prenomX = prenomX;
        this.playerO = playerO;
        this.nomO = nomO;
        this.prenomO = prenomO;
        this.winner = winner;
        this.idGame = idGame;
    };

    /**
     * Une fonction pour tester l'état de la partie
     * Détermine le gagnant ou décrète Match nul
     * Cette méthode agit sur le DOM
     * 
     * @param {string} $gridState 
     */
    isPlayerWin($gridState) {
        // Des regex pour les états gagnants
        // Etats pour lesquels X a gagné
        let xWins = [/XXX....../,/...XXX.../,/......XXX/,/X..X..X../,/.X..X..X./,/..X..X..X/,/X...X...X/,/..X.X.X../];
        // Etats pour lesquels O a gagné
        let oWins = [/OOO....../,/...OOO.../,/......OOO/,/O..O..O../,/.O..O..O./,/..O..O..O/,/O...O...O/,/..O.O.O../];
        // une variable pour factoriser le message de fin
        let isEnd = false;
    
        // On vérifie si le joueur X gagne
        xWins.forEach((regex)=>{
            if ($gridState.match(regex)) {
                // tmpName
                document.querySelector('#feedbackMsgGameZone').innerHTML = "<span class=\"player"+this.activePlayer+"\">Joueur X, "+this.prenomX+" "+this.nomX+", gagne !</span>"
                document.querySelector('#feedbackMsgGameZone').dataset.winner = 'X';
                // On gère le css de la grid
                document.querySelector('#grid').classList.remove('cross');
                document.querySelector('#grid').classList.remove('circle');
                document.querySelector('#grid').classList.add('theend');                
                // On met à jour les propriétés de la class
                this.isActiveGame = false;
                this.winner = this.playerX;
                isEnd = true;
            } 
        });
        
        // On vérifie si le joueur O gagne
        oWins.forEach((regex)=>{
            if ($gridState.match(regex)) {
                // tmpName
                document.querySelector('#feedbackMsgGameZone').innerHTML = "<span class=\"player"+this.activePlayer+"\">Joueur O, "+this.prenomO+" "+this.nomO+", gagne !</span>"
                document.querySelector('#feedbackMsgGameZone').dataset.winner = 'O';
                // On gère le css de la grid
                document.querySelector('#grid').classList.remove('cross');
                document.querySelector('#grid').classList.remove('circle');
                document.querySelector('#grid').classList.add('theend');                
                // On met à jour les propriétés de la class
                this.isActiveGame = false;
                this.winner = this.playerO;
                isEnd = true;
            } 
        });
        
        // On vérifie si le match est nul
        if ($gridState.indexOf("_") == -1) {
            document.querySelector('#feedbackMsgGameZone').innerHTML = "<span>Match nul.</span>"
            document.querySelector('#feedbackMsgGameZone').dataset.winner = "Match nul.";
            // On gère le css de la grid
            document.querySelector('#grid').classList.remove('cross');
            document.querySelector('#grid').classList.remove('circle');
            document.querySelector('#grid').classList.add('theend');            
            // On met à jour les propriétés de la class
            this.isActiveGame = false;
            this.winner = 'Match nul.';
            isEnd = true;
        };
        // On met à jour le message
        if (isEnd) {
            document.querySelector('#msgAccueil').innerHTML = `<h3>La partie <i>${this.idGame.slice(0,-4)}</i> est finie.</h3>`;    
        };        
    };

    /**
     * Un string étant immuable
     * Une fonction pour remplacer un élément à une place donnée dans un string
     * Cette méthode sert à mettre à jour l'état d'une partie avant de la transmettre à la BDD
     * @param {string} str 
     * @param {number} index 
     * @param {char} char 
     * @returns le string avec le caractère remplacé
     */
    strReplaceAt(str, index, char) {
        // On explose le string    
        let explodedStr = str.split("");
        // On remplace
        explodedStr[index] = char;
        // On reforme le string
        return explodedStr.join("");
    };

    /**
     * Pour lancer la synchro BDD via le sript php ad hoc
     * 
     * @param {string} url - url du script php
     * @param {object} datas - un objet avec les éventuelles données à transmettre au script php
     * @param {string} activePlayer - le symbole X ou O du joueur actif
     */
    fetchMajState(url,datas,activePlayer) {
        console.log('fecth Maj State'); // Pour mon info dans la console        
        // let logPlayer = this.logPlayer; // Ne sert pas ici
        this.currentGameState = datas.currentState;        

        console.log('C\'est au joueur ',activePlayer,' de jouer.')
        fetch(url, {
            method: "POST",
            credentials: "include",
            body: JSON.stringify(datas),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        })
            .then((response) => response.json())
            .then((datas) => {
                // On met à jour le zone de feedbacks sous la grid tant que le jeu est actif                
                let tmpName = datas.activePlayer == 'O' ? (datas.prenomO+' '+datas.nomO) : (datas.prenomX+' '+datas.nomX);                
                document.querySelector('#feedbackMsgGameZone').innerHTML = "<span class=\"player"+datas.activePlayer+"\">C'est au joueur "+datas.activePlayer+", "+tmpName+", de jouer.</span>";
                document.querySelector('#feedbackMsgGameZone').dataset.currentplayer = datas.activePlayer;
                // Si l'état du jeu n'est pas "_________" on change l'affichage du message au dessus de lagrid

                
            })
            .catch((datas) => {
                console.error("Error : ",datas.message)
            });
    };
}

export { Game } 