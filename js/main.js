//=================================================================
// SCRIPT PRINCIPAL DE GESTION DE L'APLLICATION
//
// Gestion de l'application via fetch API
//
// @author Sébastien LOZANO
//
//=================================================================

// On importe les methodes necessaires
import { calcMD5 } from './modules/md5.js';
import { Profil } from './modules/profil.js';
import { DOMRegex, toggleId } from './modules/domTools.js';
import { Game } from './modules/game.js';


// On récupère les éléments du DOM utiles dans des variables
// Formulaire de connexion joueur
let formConnexion = document.querySelector('#formConnexion');
let msgConnexionKO = document.querySelector('#msgConnexionKO');
let msgConnexionOK = document.querySelector('#msgConnexionOK');
let divConnexion = document.querySelector('#connexion');
// Formulaire d'inscription nouveau joueur
let formInscription = document.querySelector('#formInscription');
let msgInscriptionKO = document.querySelector('#msgInscriptionKO');
let msgInscriptionOK = document.querySelector('#msgInscriptionOK');
let divInscription = document.querySelector('#inscription');
// Formulaire de modification compte joueur
let formModification = document.querySelector('#formModification');
let msgModificationKO = document.querySelector('#msgModificationKO');
let msgModificationOK = document.querySelector('#msgModificationOK');
let divModification = document.querySelector('#modification');
// Formulaire de création d'une partie
let formCreateGame = document.querySelector('#formCreateGame');
let divCreateGame = document.querySelector('#createGame');
let msgCreateGameKO = document.querySelector('#msgCreateGameKO');
let msgCreateGameOK = document.querySelector('#msgCreateGameOK');
// La partie hero avec le message d'accueil et la partie réservée au mode connecté
let msgAccueil = document.querySelector('#msgAccueil');
let sessionOKProfil = document.querySelector('#sessionOKProfil');
let sessionOKStuffs = document.querySelector('#sessionOKStuffs');
let sessionOKProfilFeedbacks = document.querySelector('#sessionOKProfilFeedbacks');
// La div de documentation et les élements pour la documentation
let divDocumentation = document.querySelector('#documentation');
let docInscriptionConnexion = document.querySelector('#docInscriptionConnexion');
let docConnectedMode = document.querySelector('#docConnectedMode');
let docAccueil = document.querySelector('#docAccueil');
let docModifProfil = document.querySelector('#docModifProfil');
let docShowStats = document.querySelector('#docShowStats');
let docShowGames = document.querySelector('#docShowGames');
let docCreateGame = document.querySelector('#docCreateGame');
// La div de zone de jeu
let divGameZone = document.querySelector('#gameZone');
let sectionGrid = document.querySelector('#grid');
let feedbackMsgGameZone = document.querySelector('#feedbackMsgGameZone');
// Un booléen pour éviter d'attacher plusieurs listener au même evenement
let myGlobalListenerBool = false;
// Les boutons et liens
let btnDeconnexion = document.querySelector('#deconnexion');
let btnShowConnexion = document.querySelector('#showConnexion');
let btnShowInscription = document.querySelector('#showInscription');
let btnShowModification = document.querySelector('#showModification');
let btnShowModificationAvatarPerso = document.querySelector('#showModificationAvatarPerso');
let btnShowDocumentation = document.querySelector('#showDocumentation');
let btnDocInscriptionConnexion = document.querySelector('#btnDocInscriptionConnexion');
let btnDocConnectedMode = document.querySelector('#btnDocConnectedMode');
let btnDocAccueil = document.querySelector('#btnDocAccueil');
let btnDocModifProfil = document.querySelector('#btnDocModifProfil');
let btnDocShowStats = document.querySelector('#btnDocShowStats');
let btnDocShowGames = document.querySelector('#btnDocShowGames');
let btnDocCreateGame = document.querySelector('#btnDocCreateGame');
let btnModificationAbandon = document.querySelector('#modificationAbandon');
let btnPageName = document.querySelector('#pageName');
let btnMyGames = document.querySelector('#myGames');
let btnNewGame = document.querySelector('#newGame');
let btnStatsStuffs = document.querySelector('#statsStuffs');
let btnStatsNav = document.querySelector('#statsNav');
let btnLoginInfos = document.querySelector('#loginInfos');
// On réunit les id des btn et liens dans un tableau
let btnId = ['deconnexion','showConnexion','showInscription','showModification','showDocumentation','modificationAbandon','pageName','myGames','newGame','statsStuffs','statsNav','loginInfos','btnDocInscriptionConnexion','btnDocConnectedMode'];
// Un booléen pour savoir si l'utilisateur va uploder son avatar perso
let isAvatarPerso = false;
// Une variable globale pour le profil
var currentProfil;
// Une variable globale pour le jeu courant
var currentGame;
// Le necessaire pour la modale
let modal = document.querySelector(".modal");
let closeButton = document.querySelector(".closeButton");
let modalBody = document.querySelector('#modalBody');
let modalFeedbacks = document.querySelector('#modalFeedbacks');
let modalValidDelete = document.querySelector('#modalValidDelete');
let modalAbortDelete = document.querySelector('#modalAbortDelete');
let modalTimer;

//======================================================================================
//
// FONCTIONS
//
//======================================================================================

/**
 * Une fonction d'initialisation pour l'authentification
 * 
 * @author Sébastien LOZANO
 */
function initAuth() {
    console.log('Execution de initAuth()');  
    // On cache le bouton de deconnexion
    btnDeconnexion.style.display = 'none';

    // On gère les affichages pour la connexion
    divConnexion.style.display = 'none';
    msgConnexionOK.style.display = 'none';
    msgConnexionKO.style.display = 'none';

    // On gère les affichages pour l'inscription
    divInscription.style.display = 'none';
    msgInscriptionOK.style.display = 'none';
    msgInscriptionKO.style.display = 'none';

    // On gère les affichages pour la modification
    divModification.style.display = 'none';
    msgModificationOK.style.display = 'none';
    msgModificationKO.style.display = 'none';

    // On gère l'affichage des Cards d'actions en mode connecté
    sessionOKStuffs.style.display = 'none';
    btnStatsNav.style.display = 'none';

    // On gère l'affichage des feedbacks en mode connecté
    sessionOKProfilFeedbacks.style.display = 'none';
    
    // On gère l'affichage pour la création d'une partie
    divCreateGame.style.display = 'none';
    msgCreateGameOK.style.display = 'none';
    msgCreateGameKO.style.display = 'none';

    // On gère l'affichage de la documentation
    divDocumentation.style.display = 'none';

    // On gère l'affichage de la zone de jeu
    divGameZone.style.display = 'none';

    // On gère l'affichage de la doc
    docInscriptionConnexion.style.display = "none";
    docConnectedMode.style.display = "none";
    docAccueil.style.display = "none";
    docModifProfil.style.display = "none";
    docShowGames.style.display = "none";
    docShowStats.style.display = "none";
    docCreateGame.style.display = "none";

    // On gère l'affichage du msg d'accueil pour le mode non connecté
    msgAccueil.style.display = 'block';
    // Attention le message a déjà peut être été modifié par un click sur connexion ou inscription
    // Donc on remet l'affichage initial
    msgAccueil.innerHTML =`
    <h3>Projet bloc4 DIU</h3>
    <div>
        <p>
        Le projet consiste à créer un espace de jeu de Tic Tac Toe (morpion) sur le web.
        Les joueurs devront pouvoir s’inscrire,	se connecter, lancer une partie, jouer (en tour par tour), et
        consulter les données (parties, membres, statistiques, etc.)
        </p>
    </div>
    <div>
        <img src="./img/tictactoe.png"/>
    </div>`;	

    // On vérifie s'il y a déjà des informations de session stockées sur le client
    if (sessionStorage.getItem('sessionOK')==='true') {    
        // On supprime le message d'accueil
        msgAccueil.innerHTML = '';
        // On gère les boutons connexion/deconnexion de la barre de navigation
        btnShowConnexion.style.display = 'none';
        btnDeconnexion.style.display = 'initial';
        // Le bouton d'inscription disparait aussi en mode connecté
        btnShowInscription.style.display = 'none';
        // Le bouton d'affichage des statistiques apparaît dans la barre de navigation en mode connecté
        btnStatsNav.style.display = 'initial';
        // On ajoute la div sessionOK au DOM au cas où ...
        sessionOKProfil.style.display = 'block';        
        sessionOKStuffs.style.display = 'block';        
        // On recupère les infos sur le profil stockées sur le client        
        currentProfil = JSON.parse(sessionStorage.getItem('currentProfil'));
        // On a besoin des methodes de la class Profil
        currentProfil = new Profil(currentProfil.login, currentProfil.avatar, currentProfil.nom, currentProfil.prenom, currentProfil.parties, currentProfil.statistiques);                    
        // On affecte le contenu de la div pricipale avec un message de bienvenue
        sessionOKProfil.innerHTML = currentProfil.bienvenue('Bienvenue') + currentProfil.msgAfterConnexion;
        // On ajoute l'avatar et le login dans la barre de Nav
        btnLoginInfos.innerHTML = `<img class="imgAvatarNav" src="${currentProfil.avatar}"/> ${currentProfil.prenom} ${currentProfil.nom}`;        
    };
};

/**
 * Une fonction pour inititialiser les display et nettoyer sessionStorage
 * 
 * @param {object} divToDisplay - le querySelector de la div à afficher
 * 
 * @author Sébastien LOZANO
 */
function initDisplays(divToDisplay) {    
    // On cache tout
    divConnexion.style.display = 'none'; 
    divInscription.style.display = 'none';
    divModification.style.display = 'none';
    divDocumentation.style.display = 'none';
    divCreateGame.style.display = 'none';
    divGameZone.style.display = 'none';
    // On affiche seulement la div ad hoc
    if (!(typeof(divToDisplay) === 'undefined')) {
        divToDisplay.style.display = 'block';
    };    
    // On montre la div message d'accueil pour le cas de click en mode non connecté
    msgAccueil.style.display = 'block';    
    // On supprime la variable sessionOK de sessionStorage si elle est à false, sinon on en a besoin !
    if (sessionStorage.getItem('sessionOK') === 'false') {
        sessionStorage.removeItem('sessionOK');
    };           
    
}; 

/**
 * Une fonction pour réinitialiser les formulaires et montrer/cacher les div ad hoc 
 * 
 * @param {string} strZone - contient une chaine concernant la gestion de l'affichage à faire, 
 * connexion/inscription/modification/modificationAbandon/documentation/statistiquesNav/statistiquesStuffs/logInfos
 * 
 * @author Sébastien LOZANO
 */
function cleanAndShow(strZone) {
    let dataMajStatistiques = {};
    switch (strZone) {
        case 'connexion':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console
            // On initialise les affichages
            initDisplays(divConnexion);
            // On réinitialise le formulaire d'inscription    
            document.querySelector('#loginConnexion').value = "";
            document.querySelector('#pwdConnexion').value = "";   
            // On cache les div de message d'erreur/succés
            msgConnexionKO.style.display = 'none';                    
            msgConnexionOK.style.display = 'none';
            // On met à jour la zone msgAccueil            
            msgAccueil.innerHTML =`<h3>Compléter le formulaire pour accéder à votre espace.</h3>`;
            break;
        case 'inscription':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console
            // On initialise les affichages
            initDisplays(divInscription);
            // On réinitialise le formulaire d'inscription
            document.querySelector('#loginInscription').value = "";
            document.querySelector('#pwdInscription1').value = "";
            document.querySelector('#pwdInscription2').value = "";
            document.querySelector('#nomInscription').value = "";
            document.querySelector('#prenomInscription').value = "";
            document.querySelector('#avatarInscription1').checked = true;
            // On cache les div de message d'erreur/succés
            msgInscriptionKO.style.display = 'none';                    
            msgInscriptionOK.style.display = 'none'; 
            // On met à jour la zone msgAccueil                        
            msgAccueil.innerHTML =`<h3>Compléter le formulaire pour vous inscrire.</h3>`;
            break;
        case 'modification':
            strZone = isAvatarPerso ? strZone + ' avec avatar perso' : strZone;
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console
            // On initialise les affichages
            initDisplays(divModification);
            // On pré-rempli le formulaire de modification avec les éléments de la base
            document.querySelector('#loginModification').value = currentProfil.login;
            document.querySelector('#pwdModification1').value = "";
            document.querySelector('#pwdModification2').value = "";
            document.querySelector('#nomModification').value = currentProfil.nom;
            document.querySelector('#prenomModification').value = currentProfil.prenom;            
            if (isAvatarPerso) {
                document.querySelector('#avatars').style.display = 'none';
                document.querySelector('#avatarPerso').style.display = 'block';
            } else {
                document.querySelector('#avatars').style.display = 'block';
                document.querySelector('#avatarPerso').style.display = 'none';
                // On réinitialise/met à jour le choix des avatars au cas où on vienne de le modifier
                chooseAvatars();
            };
            // On cache les div de message d'erreur/succés
            msgModificationKO.style.display = 'none';                    
            msgModificationOK.style.display = 'none'; 
            // On remet le texte Abandon sur le bouton d'abandon
            // Il peut être à Fermer si le formulaire de modification a déjà été uilisé dans la session courante.
            btnModificationAbandon.value = "Abandon";
            // On met à jour la zone sessionOKProfil
            sessionOKProfil.style.display = 'block'; // Au cas où si elle avait était cachée
            sessionOKProfil.innerHTML = `<h3>Compléter le formulaire pour modifier le compte.</h3>`;
            break;
        case 'modificationAbandon':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console
            // On initialise les affichages
            initDisplays()
            // On met à jour la zone sessionOKProfil
            sessionOKProfil.style.display = 'block'; // Au cas où si elle avait était cachée
            // On ajoute la div de feedbacks au DOM
            sessionOKProfilFeedbacks.style.display = 'block';
            // Si la div de feedbacks ne contient pas msg, on supprime tout et on l'ajoute
            if (!sessionOKProfilFeedbacks.classList.contains('great')) {
                sessionOKProfilFeedbacks.className = "";
                sessionOKProfilFeedbacks.classList.add('great');
            };                                    
            // On la sort du DOM au bout de 3 secondes
            window.setTimeout(function(){
                sessionOKProfilFeedbacks.style.display = 'none';
            },3000);            
            if (btnModificationAbandon.value == 'Abandon') {
                sessionOKProfilFeedbacks.innerHTML = `<strong>✌Yep✌</strong> Abandon des modifications OK !`;                
            } else {
                sessionOKProfilFeedbacks.innerHTML = `<strong>✌Yep✌</strong> Modifications OK - Fermeture du module de modification du profil !`;                
            }
            sessionOKProfil.innerHTML = currentProfil.msgAfterConnexion;
            break;
        case 'documentation':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console
            // On initialise les affichages
            initDisplays(divDocumentation);
            // On cache la div message d'accueil pour le cas de click en mode non connecté
            msgAccueil.style.display = 'none';
            // On cache la div de sessionProfil
            sessionOKProfil.style.display = 'none';                
            break;
        // Factorisation des deux 'case' suivants
        case 'statistiquesNav':
        case 'statistiquesStuffs':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console            
            // On initialise les affichages
            initDisplays();
            // On cache la div message d'accueil pour le cas de click en mode non connecté
            msgAccueil.style.display = 'none';
            // On met à jour les stats du profil courant au cas où un autre utilisateur ait fait une manip
            // On va redefinir le profil courant
            currentProfil = new Profil(currentProfil.login, currentProfil.avatar, currentProfil.nom, currentProfil.prenom, currentProfil.parties, currentProfil.statistiques);                    
            dataMajStatistiques = {                
            };
            // La div sessionOKProfil est géré via la methode suivante, affichage + contenu
            currentProfil.fetchMajStatistiques("./espaceMembres/majStatistiquesFetch.php",dataMajStatistiques,strZone)
            break;
        case 'loginInfos':
            console.log('Affichage',strZone,'OK.'); // Pour mon info dans la console            
            // On initialise les affichages
            initDisplays();
            // On cache la div message d'accueil pour le cas de click en mode non connecté
            msgAccueil.style.display = 'none';
            // On met à jour la zone sessionOKProfil
            sessionOKProfil.style.display = 'block'; // Au cas où si elle avait était cachée
            sessionOKProfil.innerHTML = `                
                <div class="row">
				<div class="col c4 card">
					<h3>Infos sur le compte</h3>
					<ul>
						<li><b>Avatar</b> : <img class="imgAvatarNav" src="${currentProfil.avatar}"/> </li>
						<li><b>Prénom</b> : ${currentProfil.prenom}</li>
                        <li><b>Nom</b>    : ${currentProfil.nom}</li>
                        <li><b>Login</b>  : ${currentProfil.login}</li>
					</ul>										
				</div>
            `;
            break;
        default:
            console.log(`Problème : le cas ${strZone} n'est pas prévu.`)
    }
}

/**
 * Une fonction pour proposer un choix d'avatars à la modification du profil et réafficher l'avatar de l'utyilisateur connecté
 */
function chooseAvatars() {
    // On réinitialise/met à jour le choix des avatars au cas où on vienne de le modifier                    
    document.querySelector('#avatarModification1').value = currentProfil.avatar;
    document.querySelector('#avatarModificationImg').src = currentProfil.avatar;
    document.querySelector('#avatarModification1').checked = true;
    // Il faut récupérer deux avatars distincts de l'actuel via un fecth
    let dataAvatar = {
        currentAvatar: currentProfil.avatar
    };
    // On va traiter la modification via PHP sur le serveur
    fetch("./espaceMembres/propositionAvatarFetch.php", {
        method: 'POST',
        credentials: "include",
        body: JSON.stringify(dataAvatar),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((dataAvatar) => { 
            // document.querySelector('#avatarModification2').value = document.querySelector('#avatarModification2').value;
            // document.querySelector('#avatarModificationImg2').src = document.querySelector('#avatarModificationImg2').src;    
            // document.querySelector('#avatarModification3').value = document.querySelector('#avatarModification3').value;
            // document.querySelector('#avatarModificationImg3').src = document.querySelector('#avatarModificationImg3').src;
            document.querySelector('#avatarModification2').value = dataAvatar.avatar2;
            document.querySelector('#avatarModificationImg2').src = dataAvatar.avatar2;    
            document.querySelector('#avatarModification3').value = dataAvatar.avatar3;
            document.querySelector('#avatarModificationImg3').src = dataAvatar.avatar3;
                
            console.log(JSON.stringify(dataAvatar));
        })
        .catch((dataAvatar) => {
            console.error("Error : ",dataAvatar.message)
        });
}

/**
 * Une fonction pour afficher une partie et on gère la mise à jour de son état enregistré sur la BDD à chaque seconde
 * 
 * @param {string} idGame - l'identifiant unique d'une partie tel que stocké dans la BDD
 * 
 * @author Sébastien LOZANO
 */
function showGame(idGame) {
    // On cache la div du profil
    sessionOKProfil.style.display = 'none';
    // On affiche la zone de jeu
    divGameZone.style.display = 'block';
    // On supprime les classes éventuellement attachées au la section #grid
    sectionGrid.classList.remove('cross');
    sectionGrid.classList.remove('circle');             
    sectionGrid.classList.remove('theend');
    // On supprime les feedbacks et les dataset de la div des feedbacks
    feedbackMsgGameZone.innerHTML = '';
    feedbackMsgGameZone.dataset.currentplayer = '';
    feedbackMsgGameZone.dataset.winner = '';
    // On instancie un objet de la class Game
    currentGame = new Game();
    // On définit ses propriétés idGame pour l'identifiant de la partie
    // et logPlayer pour le login du joueur connecté 
    currentGame.idGame = idGame;
    currentGame.logPlayer = currentProfil.login;                                        
    // On récupère l'attribut de données data-action du lien cliqué
    let dataAction = document.querySelector('#'+idGame).dataset.action;     
    // On adapte le message d'accueil
    let textToShow;
    switch (dataAction) {
        case 'Affichage':
            textToShow = `la partie <i>${idGame.slice(0,-4)}</i> est finie.`;
            break;
        case 'Reprise':
            textToShow = `la partie <i>${idGame.slice(0,-4)}</i> est en cours...`;
            break;
        case 'Demarrage':
            textToShow = `on démarre la partie <i>${idGame.slice(0,-4)}</i>.`;
            break;
        default:
            textToShow = `Bug, cas non prévu !`;
            break;
    };
    msgAccueil.style.display = 'block'; // Au cas où ça ne soit pas dans le DOM    
    msgAccueil.innerHTML = `<h3>Yo ${currentProfil.prenom} ${currentProfil.nom}, ${textToShow}</h3>`;    
    // On récupère les éléments dans la BDD
    // On met à jour la grid dans son état stocké dans la BDD
    let dataMajGame = {
        idGame: currentGame.idGame,
        isActiveGame: currentGame.isActiveGame,
        //currentState: currentGame.currentGameState,
        activePlayer: currentGame.activePlayer,
        playerX: currentGame.playerX,
        playerO: currentGame.playerO,
        winner: currentGame.winner,
        logPlayer: currentGame.logPlayer
    };     
    fetch("./espaceMembres/gameInitStateFetch.php", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(dataMajGame),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((dataMajGame) => {
            console.log('Mise à jour de la grid de la partie'); // Pour mon info dans la console            
            // On met à jour currentGame avec l'etat du jeux en BDD, le playerX, le playerO, le symbol du joueur actif
            currentGame.currentGameState = dataMajGame.currentState;
            currentGame.playerX = dataMajGame.playerX;
            currentGame.nomX = dataMajGame.nomX;
            currentGame.prenomX = dataMajGame.prenomX;
            currentGame.playerO = dataMajGame.playerO;
            currentGame.nomO = dataMajGame.nomO;
            currentGame.prenomO = dataMajGame.prenomO;
            currentGame.activePlayer = dataMajGame.activePlayer;            
            // On affiche le premier état du jeu
            for (let i= 0; i< dataMajGame.currentState.length; i++) {
                document.querySelectorAll('.case').forEach( cell => {
                    if (cell.dataset.index == i) {
                        switch (dataMajGame.currentState[i]) {
                            case 'X':
                                cell.innerHTML = '<img width="60" height="60" src="./img/cross.png"/>';
                                break;
                            case 'O':
                                cell.innerHTML = '<img width="60" height="60" src="./img/circle.png"/>';
                                break;
                            case '_':
                                cell.innerHTML = '';
                                break;
                        }                                    
                    };
                });
            };
            // On affecte ce qu'il faut en fonction de l'état du jeu
            // Si c'est une nouvelle partie, cross est affecté par défaut dans le template index.php
            switch (dataMajGame.symbolGrid) {
                case 'cross':
                    sectionGrid.classList.remove('circle');
                    sectionGrid.classList.add('cross');
                    break;
                case 'circle':
                    sectionGrid.classList.remove('cross');
                    sectionGrid.classList.add('circle');
                    break;
                default:
                    console.log('dataMajGame.symbolGrid : ', dataMajGame.symbolGrid);
            };
            // On vérifie si la partie est finie
            currentGame.isPlayerWin(currentGame.currentGameState);
            // On ajoute le joueur actif au feedback si la partie est active
            if (currentGame.isActiveGame) {                
                let tmpName = currentGame.activePlayer == 'O' ? (currentGame.prenomO+' '+currentGame.nomO) : (currentGame.prenomX+' '+currentGame.nomX);                
                feedbackMsgGameZone.innerHTML = "<span class=\"player"+currentGame.activePlayer+"\">C'est au joueur "+currentGame.activePlayer+", "+tmpName+", de jouer.</span>";
                feedbackMsgGameZone.dataset.currentplayer = currentGame.activePlayer;    
            };
            // On ajoute les listeners utiles sur la grid
            if (!myGlobalListenerBool) {
                myGlobalListenerBool = true;
                document.querySelectorAll(".case").forEach(cell => cell.addEventListener("click",clickManagement));
            };

            // On gère le clic sur la grid
            function clickManagement() {
                console.log('Clic sur une case de la grid de jeu'); // Pour mon info dans la console
                // Au premier click on met à jour le message sauf si la partie est finie
                if (currentGame.isActiveGame) {
                    msgAccueil.innerHTML = `<h3>Yo ${currentProfil.prenom} ${currentProfil.nom}, la partie <i>${currentGame.idGame.slice(0,-4)}</i> est en cours...</h3>`;   
                };                
                // On définit deux booléens pour bloquer le joueur qui ne doit pas jouer
                let myPlayerXBool = (currentGame.activePlayer == 'X' && currentGame.logPlayer == currentGame.playerX );
                let myPlayerOBool = (currentGame.activePlayer == 'O' && currentGame.logPlayer == currentGame.playerO );
                /**
                 * Une fonction locale pour factoriser la mise à jour de la grid
                 * @param {obj} thisCaseClicObj - L'objet JS this de la case cliquée
                 * @param {string} symb - Le calss css de la grid au moment du clic
                 */
                function majGrid(thisCaseClicObj,symbText) {
                    let myObj = {
                        mySymb: '',
                        vsSymb: '',
                        img: '',
                        vsSymbText: ''
                    };

                    switch (symbText) {
                        case 'cross':
                            myObj.mySymb = 'X';
                            myObj.vsSymb = 'O';
                            myObj.img = '<img width="60" height="60" src="./img/cross.png"/>';
                            myObj.vsSymbText = 'circle';
                            break;
                        case 'circle':
                            myObj.mySymb = 'O';
                            myObj.vsSymb = 'X';
                            myObj.img = '<img width="60" height="60" src="./img/circle.png"/>';
                            myObj.vsSymbText = 'cross';
                            break;
                    };                    
                    thisCaseClicObj.innerHTML = myObj.img;
                    sectionGrid.classList.remove(symbText);
                    sectionGrid.classList.add(myObj.vsSymbText);
                    // On modifie l'état de la grid de jeu
                    currentGame.currentGameState = currentGame.strReplaceAt(currentGame.currentGameState,indexCase,myObj.mySymb);
                    // On change le joueur actif 
                    currentGame.activePlayer = myObj.vsSymb;
                    // On vérifie si un joueur a gagné
                    currentGame.isPlayerWin(currentGame.currentGameState);
                    // On met à jour la BDD
                    let dataMajGame = {
                        idGame: currentGame.idGame,
                        isActiveGame: currentGame.isActiveGame,
                        currentState: currentGame.currentGameState,
                        activePlayer: currentGame.activePlayer,
                        playerX: currentGame.playerX,
                        nomX: currentGame.nomX,
                        prenomX: currentGame.prenomX,
                        playerO: currentGame.playerO,
                        nomO: currentGame.nomO,
                        prenomO: currentGame.prenomO,
                        winner: currentGame.winner,
                        logPlayer: currentGame.logPlayer
                    };                                        
                    // Si le jeu n'est pas fini, on change le message qui indique le joueur actif
                    // Et On fetch via la méthode de la classe Game de ./js/modules/game.js
                    if (currentGame.isActiveGame) {                        
                        // La mise à jour de la div de feedbacks se fait dans la méthode fetchMajState() de la class Game()                   
                        currentGame.fetchMajState("./espaceMembres/gameMajStateFetch.php",dataMajGame,currentGame.activePlayer);                                    
                    } else {                        
                        // La mise à jour de la div de feedbacks se fait dans la méthode isPlayerWin() de la class Game()
                        currentGame.fetchMajState("./espaceMembres/gameMajStateFetch.php",dataMajGame,currentGame.activePlayer);                            
                    };
                };                                    
                // On récupère l'index de la case cliquée
                const indexCase = parseInt(this.dataset.index)                                    
                // Si la case est vide et que le jeu est actif
                if (this.dataset.value == "_" && currentGame.isActiveGame) {                            
                    // On verifie la class de la section grid pour ajouter une croix ou un cercle
                    if (sectionGrid.classList.contains("circle") && myPlayerOBool) {                                                
                        majGrid(this,'circle');
                    }; 
                    if (sectionGrid.classList.contains("cross") && myPlayerXBool) {
                        majGrid(this,'cross');
                    };
                };                
            };
        })
        .catch((dataMajGame) => {
            console.error("Error : ",dataMajGame.message)
        });
};

/**
 * Une fonction pour ajouter les listeners au tableau des parties
 * 
 * @param {string} logPlayer - une chaine de caractères avec le login du joueur connecté
 * 
 * @author Sébastien LOZANO
 */
function addMyListenersGamesTab(logPlayer){
    // On ajoute des listeners au besoin
    let idPartiesTab = [];
    DOMRegex(/_vs_/).forEach( e => {
        // On récupère les id dans un tableau    
        idPartiesTab.push(e.getAttribute("id"));
    });            
    // On ajoute un listener sur chaque lien dont l'id est dans le tableau
    idPartiesTab.forEach( e => {            
        // On test si la chaine d'attribut id contient suppr
        if (e.indexOf("suppr") === -1) { // si non c'est pour de l'affichage
            document.querySelector('#'+e).addEventListener('click', event => {
                // On empeche le comportement par défaut
                event.preventDefault();
                showGame(e);
                // un booléen pour stopper la synchroniqtion BDD
                let stopFetch = false;
                // Une fois la partie affichée, on lance une verif régulière
                // Toutes les secondes, de l'état de la partie sur la BDD
                var checkChange = function(){
                    let dataMajGame = {
                        idGame: e,
                        logPlayer: logPlayer// dataMajParties.logPlayer
                    }; 
                    fetch("./espaceMembres/gameInitStateFetch.php", {
                        method: "POST",
                        credentials: "include",
                        body: JSON.stringify(dataMajGame),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                        },
                    })
                        .then((response) => response.json())
                        .then((dataMajGame) => {
                            console.log(`Mise à jour de l'état de la partie ${dataMajGame.idGame} en cours ...`); // Pour mon info dans la console                                     
                            // Si cette propriété au retour du fetch n'est pas vide c'est que le nom de la partie n'a pas été trouvé
                            if (dataMajGame.msgGameKO == '') {
                                // On met à jour currentGame avec l'etat du jeux en BDD, le playerX, le playerO, le symbol du joueur actif
                                currentGame.currentGameState = dataMajGame.currentState;
                                currentGame.playerX = dataMajGame.playerX;
                                currentGame.nomX = dataMajGame.nomX;
                                currentGame.prenomX = dataMajGame.prenomX;
                                currentGame.playerO = dataMajGame.playerO;
                                currentGame.nomO = dataMajGame.nomO;
                                currentGame.prenomO = dataMajGame.prenomO;
                                currentGame.activePlayer = dataMajGame.activePlayer;                                    
                                // On affiche le premier état du jeu
                                for (let i= 0; i< dataMajGame.currentState.length; i++) {
                                    document.querySelectorAll('.case').forEach( cell => {
                                        if (cell.dataset.index == i) {
                                            switch (dataMajGame.currentState[i]) {
                                                case 'X':
                                                    cell.innerHTML = '<img width="60" height="60" src="./img/cross.png"/>';
                                                    break;
                                                case 'O':
                                                    cell.innerHTML = '<img width="60" height="60" src="./img/circle.png"/>';
                                                    break;
                                                case '_':
                                                    cell.innerHTML = '';
                                                    break;
                                            }                                    
                                        };
                                    });
                                };
                                // On affecte ce qu'il faut en fonction de l'état du jeu
                                // Si c'est une nouvelle partie, cross est affecté par défaut dans le template index.php
                                switch (dataMajGame.symbolGrid) {
                                    case 'cross':
                                        sectionGrid.classList.remove('circle');
                                        sectionGrid.classList.add('cross');
                                        break;
                                    case 'circle':
                                        sectionGrid.classList.remove('cross');
                                        sectionGrid.classList.add('circle');
                                        break;
                                    default:
                                        console.log('dataMajGame.symbolGrid : ', dataMajGame.symbolGrid);
                                };
                                // On ajoute la class .wait à la grid de jeu 
                                // En fonction du joueur connecté, de son symbole et du symbol dont c'est le tour de jeu
                                if (dataMajGame.logPlayer == dataMajGame.playerO && dataMajGame.symbolGrid == 'cross') {
                                    sectionGrid.classList.add('wait')
                                };
                                if (dataMajGame.logPlayer == dataMajGame.playerX && dataMajGame.symbolGrid == 'cross') {
                                    sectionGrid.classList.remove('wait')
                                };
                                if (dataMajGame.logPlayer == dataMajGame.playerX && dataMajGame.symbolGrid == 'circle') {
                                    sectionGrid.classList.add('wait')
                                };
                                if (dataMajGame.logPlayer == dataMajGame.playerO && dataMajGame.symbolGrid == 'circle') {
                                    sectionGrid.classList.remove('wait')
                                };
                                // Si lapartie est finie on supprime la class .wait de la grid de jeu
                                if (sectionGrid.classList.contains('theend')) {
                                    sectionGrid.classList.remove('wait')
                                };
                                // On vérifie si la partie est finie
                                currentGame.isPlayerWin(currentGame.currentGameState);                                    
                                // On ajoute le joueur actif au feedback si la partie est active
                                if (currentGame.isActiveGame) {
                                    let tmpName = currentGame.activePlayer == 'O' ? (currentGame.prenomO+' '+currentGame.nomO) : (currentGame.prenomX+' '+currentGame.nomX);
                                    feedbackMsgGameZone.innerHTML = "<span class=\"player"+currentGame.activePlayer+"\">C'est au joueur "+currentGame.activePlayer+", "+tmpName+", de jouer.</span>";
                                    feedbackMsgGameZone.dataset.currentplayer = currentGame.activePlayer;    
                                };
                                // Si on clique sur un bouton quelconque sauf celui qui lance la partie :
                                // 1/ il faut stopper la synchro BDD actuelle
                                // 2/ il faut gérer l'affichage
                                // Toutes les id des boutons à gérer sont dans un tableau déclarer au debut de ce script                                
                                btnId.forEach( id => {
                                    document.querySelector('#'+id).addEventListener("click", (e)=>{
                                        stopFetch = true;
                                        cleanAndShow(id);
                                    });
                                });
                            } else {
                                console.log(dataMajGame.msgGameKO,' La partie a dû être supprimée, on stoppe la routine de synchro avec la BDD.'); // Pour mon info dans la console
                                // On change le booléen qui va servir à stopper la synchro avec la BDD
                                stopFetch = true;
                            }
                        })
                        .catch((dataMajGame) => {
                            console.error("Error : ",dataMajGame.message)
                        });
                    // Si la partie n'est pas finie on relance la synchro BDD sauf si la partie n'existe plus
                    // Sinon on arrête la routine
                    if (currentGame.isActiveGame) {                                
                        if (stopFetch) {
                            // On stoppe la routine de synchro avec la BDD
                            window.clearTimeout(checkChange);
                        } else {
                            window.setTimeout(checkChange, 1000);
                        };
                    } else {
                        sectionGrid.classList.remove('wait')
                        window.clearTimeout(checkChange);
                    };
                }
                // Pour initialiser la mise à jour de la grid
                var initCheckChange = function(){
                    window.setTimeout(checkChange, 1000);
                }   
                // On lance la routine de mise à jour de la grid
                initCheckChange();
            });    
        } else { // si oui c'est pour de la suppression
            document.querySelector('#'+e).addEventListener('click', event => {
                // On empeche le comportement de soumission par défaut
                event.preventDefault();
                console.log('submit formDeleteGame'); // Pour mon info dans la console
                modalValidDelete.style.display = 'initial';     
                modalAbortDelete.style.display = 'initial';
                modalBody.innerHTML = `<strong>🕱Attention🕱</strong><br>Tu vas supprimer la partie <strong>${e.substring(5).slice(0,-4)}</strong>.<br>Sûr de vouloir faire ça ?!<br>Cette action est irréversible !`     
                modalFeedbacks.style.display = 'none';
                // Si la modale n'est pas affichée, on l'affiche
                if (!modal.classList.contains('showModal')) {                    
                    modal.classList.add('showModal');
                }; 
                //============================================================
                // On gère la suppression
                //============================================================
                // Une fonction pour l'ajout/suppression du listener
                function suppr() {
                    console.log('suppression partie '+e.substring(5).slice(0,-4)); // Pour mon info dans la console
                    // On empeche le comportement par défaut
                    event.preventDefault();
                    // On récupère le login courrant
                    let dataDeleteGame = {
                        loginCurrent: currentProfil.login,         
                        idPartie: e.substring(5)         
                    };                    
                    // On va traiter la connexion via PHP sur le serveur
                    fetch("./espaceMembres/deleteGameFetch.php", {
                        method: 'POST',
                        credentials: "include",
                        body: JSON.stringify(dataDeleteGame),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                        },
                    })
                        .then((response) => response.json())
                        .then((dataDeleteGame) => {                    
                            // On va redefinir le profil courant
                            currentProfil = new Profil(dataDeleteGame.loginCurrent, currentProfil.avatar, currentProfil.nom, currentProfil.prenom, dataDeleteGame.parties, dataDeleteGame.statistiques);                                                    
                            // On le stocke sur le client                        
                            sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
                            // Si la div de feedbacks ne contient pas warning, on supprime tout et on l'ajoute
                            if (!modalFeedbacks.classList.contains('warning')) {
                                modalFeedbacks.className = "";
                                modalFeedbacks.classList.add('warning');
                            };
                            // On cache les boutons Valider et Annuler
                            modalValidDelete.style.display = 'none';     
                            modalAbortDelete.style.display = 'none';
                            // On modifie le texte de la modale                             
                            modalBody.innerHTML = `<h3>Partie <strong>${e.substring(5).slice(0,-4)}</strong></h3>`;
                            // On affiche la div de feedback avec le texte ad hoc
                            modalFeedbacks.style.display = 'block';
                            modalFeedbacks.innerHTML = `<strong>🕱ACTION IRRÉVERSIBLE🕱</strong><br> Suppression effective !`;
                            // On met à jour le texte de la div principale
                            sessionOKProfil.innerHTML = currentProfil.bienvenue('Toujours ready') + currentProfil.parties;   
                            // Il faut remettre tous les listeners sur les boutons du tableau des parties
                            addMyListenersGamesTab(currentProfil.login);                                        
                            // On ferme la modale au bout de 4 secondes
                            if (modal.classList.contains('showModal')) {
                                modalTimer = setTimeout(function(){                                    
                                        modal.classList.remove('showModal');
                                    },4000);
                            }; 
                        })
                        .catch((dataDeleteGame) => {
                            console.error("Error : ",dataDeleteGame.message)
                        });
                    // On supprime le listener
                    modalValidDelete.removeEventListener('click', suppr);
                    modalAbortDelete.removeEventListener('click', noSuppr);
                }
                // On ajoute le listener de suppression
                modalValidDelete.addEventListener('click', suppr)
                //============================================================
                // On gère l'annulation de la suppression
                //============================================================
                // Une fonction pour l'ajout/suppression du listener
                function noSuppr() {
                    console.log(`Annulation suppresion partie ${e.substring(5).slice(0,-4)}`); // Pour mon info dans la console
                    // On empeche le comportement par défaut
                    event.preventDefault();
                    // Si la div de feedbacks ne contient pas great, on supprime tout et on l'ajoute
                    if (!modalFeedbacks.classList.contains('great')) {
                        modalFeedbacks.className = "";
                        modalFeedbacks.classList.add('great');
                    };                   
                    // On cache les boutons Valider et Annuler
                    modalValidDelete.style.display = 'none';     
                    modalAbortDelete.style.display = 'none';  
                    // On ferme la modale au bout de 4 secondes
                    modalTimer = setTimeout(function(){                                    
                        modal.classList.remove('showModal');
                    },4000);
                    // On modifie le texte de la modale
                    modalBody.innerHTML = `<h3>Partie <strong>${e.substring(5).slice(0,-4)}</strong></h3>`;
                    // On affiche le feedback dans la modale avec le texte ad hoc
                    modalFeedbacks.style.display = 'block';
                    modalFeedbacks.innerHTML = `<strong>🕱NO STRESS🕱</strong> Suppression annulée !`;
                    // On supprime le listener
                    modalAbortDelete.removeEventListener('click', noSuppr);
                    modalValidDelete.removeEventListener('click', suppr);                                       
                };
                // On ajoute le listener
                modalAbortDelete.addEventListener('click', noSuppr)
            });        
        };
    }); 
}  

/**
 * Une fonction pour fermer la modale de suppression quand on clique sur la div dont la class vaut modal
 * modal est l'élément du DOM dont la class vaut modal
 * cet élément n'existe que si le modal est affiché et contient une div modal-content pour afficher le contenu de la modale
 * @param {any} event - tout événement se produisant dans le DOM
 * 
 * @author Sébastien LOZANO
 */
function windowOnClick(event) {
    if (event.target === modal) {                                
        modal.classList.remove('showModal');
        // On annule la fermeture automatique de la modale 
        window.clearTimeout(modalTimer);        
    };
}
 /**
  * Une fonction pour gèrer la fermeture de la modale de suppression lorsqu'on clique sur la croix
  * 
  * @author Sébastien LOZANO
  */
function modalCloseButton() {    
    modal.classList.remove('showModal');
    // On annule la fermeture automatique de la modale 
    window.clearTimeout(modalTimer);    
}

//======================================================================================
//
// LISTENERS
//
//======================================================================================

//=============================MODALE DE SUPPRESSION====================================

// On gère le click sur la croix
closeButton.addEventListener("click", modalCloseButton);

// On gère le click sur la fenetre de l'application
window.addEventListener("click", windowOnClick);

//=============================BARRE DE NAVIGATION======================================

// On gère le clic sur le bouton de deconnexion de la barre de navigation
btnDeconnexion.addEventListener('click', (event)=> {
    // Si il y a des infos de session stockées sur le client, on les supprime
    if (sessionStorage.getItem('sessionOK')==='true') {
        sessionStorage.removeItem('currentProfil');
        sessionStorage.removeItem('sessionOK');        
    }
});

// On gère le click sur le bouton de connexion de la barre de navigation
btnShowConnexion.addEventListener('click', (event)=> {
    cleanAndShow('connexion');    
});

// On gère le click sur le bouton d'inscription de la barre de navigation
btnShowInscription.addEventListener('click', (event)=> {        
        cleanAndShow('inscription');
});

// On gère le click sur le bouton de documentation de la barre de navigation
btnShowDocumentation.addEventListener('click', (event)=> {        
    cleanAndShow('documentation');    
});

// On gère le click sur le nom de la page de la barre de navigation
btnPageName.addEventListener('click', (event)=> {        
    initAuth();
});

// On gère le click sur le bouton de statistiques de la barre de navigation
btnStatsNav.addEventListener('click', (event)=> {        
    cleanAndShow('statistiquesNav');
});

// On gère le click sur l'avatar et le prenomNom dans la barre de navigation en mode connecté
btnLoginInfos.addEventListener('click', (event)=> {
    cleanAndShow('loginInfos');
});

//=============================MENU DES ACTIONS EN MODE CONNECTÉ========================

// On gère le click sur le bouton "Voir mes parties" du menu des actions en mode connecté
btnMyGames.addEventListener('click', (event)=> {
    console.log('clic Voir mes parties'); // Pour mon info dans la console      
    initAuth();        
    let dataMajParties = {
        logPlayer: currentProfil.login,        
    }
    fetch("./espaceMembres/majPartiesFetch.php", {
        method: "POST",
        credentials: "include",
        body: JSON.stringify(dataMajParties),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((dataMajParties) => {
            console.log('fecth Maj Parties'); // Pour mon info dans la console
            currentProfil.parties = dataMajParties.parties;
            // On le stocke sur le client            
            sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
            // On met à jour l'affichage
            sessionOKProfil.innerHTML = currentProfil.bienvenue('À ton service') + currentProfil.parties;
            // On ajoute les listeners au tableau des parties
            addMyListenersGamesTab(dataMajParties.logPlayer);
        })
        .catch((dataMajParties) => {
            console.error("Error : ",dataMajParties.message)
        });  
});

// On gère le click sur le bouton "Créer une partie" du menu des actions en mode connecté
btnNewGame.addEventListener('click', (event)=> {   
    // On vérifie s'il y a déjà des informations de session stockées sur le client    
    if (sessionStorage.getItem('sessionOK')==='true') {
        // On cache ce dont on ne veut pas
        // On gère les affichages pour la connexion
        divConnexion.style.display = 'none';
        msgConnexionOK.style.display = 'none';
        msgConnexionKO.style.display = 'none';

        // On gère les affichages pour l'inscription
        divInscription.style.display = 'none';
        msgInscriptionOK.style.display = 'none';
        msgInscriptionKO.style.display = 'none';

        // On gère les affichages pour la modification
        divModification.style.display = 'none';
        msgModificationOK.style.display = 'none';
        msgModificationKO.style.display = 'none';
        
        // On modifie le message d'accueil
        msgAccueil.style.display = 'none';
        // On cache la div sessionOKProfil
        sessionOKProfil.style.display = 'none';
        // On cache la div de documentation
        divDocumentation.style.display = 'none';
        // On cache la div de la zone de jeu
        divGameZone.style.display = 'none';
        // On montre la div createGame
        divCreateGame.style.display = 'block';
    };
});

// On gère le click sur le bouton "Modifier le compte" du menu des actions en mode connecté
btnShowModification.addEventListener('click', (event) => {
    isAvatarPerso = false;        
    cleanAndShow('modification');
});

// On gère le click sur le bouton "Modifier le compte avatar perso" du menu des actions en mode connecté
btnShowModificationAvatarPerso.addEventListener('click', (event) => {
    isAvatarPerso = true;        
    cleanAndShow('modification');
});


// On gère le click sur le bouton "Voir les stats" du menu des actions en mode connecté
btnStatsStuffs.addEventListener('click', (event)=> {        
    cleanAndShow('statistiquesStuffs');
});

//=============================AFFFICHAGE DE LA DOCUMENTATION===========================

/**
 * Une fonction pour gérer l'affichage du menu principal de la docummentation
 * I - Inscription/Connexion
 * II- Mode connecté
 * 
 * @param {object} divToHide - le querySelector de la div à cacher
 * @param {object} divToToggle - le querySelector de la div à toggle
 * 
 * @author Sébastien LOZANO
 */
function docInitMainDisplays(divToHide,divToToggle) {
    // On cache la div à cacher    
    divToHide.style.display = 'none';
    // On vérifie si la div à cacher est la div de l'entrée II - Mode connecté du menu
    // Dans ce cas on cache tous les éléments du sous menu
    if (divToHide === docConnectedMode) {
        docInitSubDisplays();
    }
    // On toggle la div ad hoc
    toggleId(divToToggle);
};

/**
  * Une fonction pour gérer l'affichage du menu secondaire de la docummentation 
  * II- Mode connecté
  * * Accueil - Infos compte
  * * Modification du compte
  * * Affichage des statistiques
  * * Voir ses parties
  * * Créer une partie
 * @param {object} divToToggle - le querySelector de la div à toggle
 * 
 * @author Sébastien LOZANO
 */
function docInitSubDisplays(divToToggle) {
    // On définit un tableau d'objets contenant les éléments du DOM à manipuler
    let querySelTab = [docAccueil,docModifProfil,docShowGames,docShowStats,docCreateGame];
    // On cache/toggle selon la situation
    querySelTab.forEach( e => {
        if (e === divToToggle) {
            toggleId(e)
        } else {
            e.style.display = "none";
        };    
    });
};

// On gère les clicks sur I - Inscription/Connexion
btnDocInscriptionConnexion.addEventListener('click', (event)=>{    
    docInitMainDisplays(docConnectedMode,docInscriptionConnexion);
});

// On gère les clicks sur II - Mode connecté
btnDocConnectedMode.addEventListener('click', (event)=>{
    docInitMainDisplays(docInscriptionConnexion,docConnectedMode);
});

// On gère les clicks sur les 5 éléments du sous menu de II - Mode connecté
btnDocAccueil.addEventListener('click', (event)=>{    
    docInitSubDisplays(docAccueil);
});

btnDocModifProfil.addEventListener('click', (event)=>{    
    docInitSubDisplays(docModifProfil);
});

btnDocShowStats.addEventListener('click', (event)=>{    
    docInitSubDisplays(docShowStats);
});

btnDocShowGames.addEventListener('click', (event)=>{        
    docInitSubDisplays(docShowGames);
});

btnDocCreateGame.addEventListener('click', (event)=>{            
    docInitSubDisplays(docCreateGame);
});

//=============================FORMULAIRES - SOUMISSION ET BOUTONS======================

// On gère le click sur le bouton d'abandon du formulaire de modification
btnModificationAbandon.addEventListener('click', (event) => {        
    cleanAndShow('modificationAbandon');
});

// On gère la soumission du formulaire de connexion
formConnexion.addEventListener('submit', (event) => {
    // On empeche le comportement de soumission par défaut
    event.preventDefault();
    console.log('submit formConnexion'); // Pour mon info dans la console
    // On récupère les éléments à transmettre au script php
    let data = {
        loginConnexion: document.querySelector('#loginConnexion').value,
        pwdConnexion: calcMD5(document.querySelector('#pwdConnexion').value),
    };

    // On va traiter la connexion via PHP sur le serveur
    fetch("./espaceMembres/connexionFetch.php", {
        method: 'POST',
        credentials: "include",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((data) => {           
            if (data.msgConnexionKO != '') {
                msgConnexionKO.innerHTML = data.msgConnexionKO;                
                msgConnexionKO.style.display = 'block';                                    
            } else {
                msgConnexionOK.innerHTML = data.msgConnexionOK;
                msgConnexionOK.style.display = 'block';
            };
            // On récupère un booléen 
            // Si le script PHP renvoie que tout est OK pour la session alors on stocke les infos en local dans le navigateur
            // Et on affiche un message de bienvenue 
            if (data.sessionOK) {
                sessionStorage.setItem('sessionOK', data.sessionOK);
                console.log('Connexion OK');  // Pour mon info dans la console
                // On crée un objet de la classe Profil pour stocker les éléments utiles à l'affichage
                currentProfil = new Profil(data.loginConnexion, data.avatar, data.nom, data.prenom, data.parties, data.statistiques);
                // On le stocke sur le client                
                sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
                
                // On met à jour les informations sur la page
                // Boutons de la barre de navigation, message d'accueil, les actions accessibles ...
                msgAccueil.innerHTML = '';
                btnShowConnexion.style.display = 'none';
                divConnexion.style.display = 'none';
                divInscription.style.display = 'none';
                btnDeconnexion.style.display = 'initial';
                // Le bouton d'inscription disparait aussi en mode connecté
                btnShowInscription.style.display = 'none';                
                // On recharge la page afin d'avoir les variables de session php également disponibles
                location.reload();                 
            } else {
                console.log('Connexion KO');  
            };
        })
        .catch((data) => {
            console.error("Error : ",data.message)
        });    
});

// On gère la soumission du formulaire d'inscription
formInscription.addEventListener('submit', (event) => {
    // On empeche le comportement de soumission par défaut
    event.preventDefault();
    console.log('submit formInscription'); // Pour mon info dans la console
    // On récupère les éléments à transmettre au script php
    let dataInscription = {
        loginInscription: document.querySelector('#loginInscription').value,
        pwdInscription1: calcMD5(document.querySelector('#pwdInscription1').value),
        pwdInscription2: calcMD5(document.querySelector('#pwdInscription2').value),
        nomInscription: document.querySelector('#nomInscription').value,
        prenomInscription: document.querySelector('#prenomInscription').value,
        avatarInscription: document.querySelector('[name="avatarInscription"]:checked').value,
    };

    // On va traiter l'inscription via PHP sur le serveur
    fetch("./espaceMembres/inscriptionFetch.php", {
        method: 'POST',
        credentials: "include",
        body: JSON.stringify(dataInscription),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((dataInscription) => {            
            if (dataInscription.msgInscriptionKO != '') {
                msgInscriptionOK.style.display = 'none';
                msgInscriptionKO.innerHTML = dataInscription.msgInscriptionKO;                
                msgInscriptionKO.style.display = 'block';                                    
            } else {
                msgInscriptionKO.style.display = 'none';                    
                msgInscriptionOK.innerHTML = dataInscription.msgInscriptionOK;
                msgInscriptionOK.style.display = 'block';
            }
            // Si le script PHP renvoie que tout est OK pour la session alors on stocke les infos en local dans le navigateur
            // Et on affiche un message de bienvenue
            if (dataInscription.inscriptionOK) {
                console.log('inscription OK');
                let btnShowConnexion2 = document.querySelector('#showConnexion2');
                // On gère le click sur le bouton de connexion d'une inscription réussie
                btnShowConnexion2.addEventListener('click', (event)=> {
                    cleanAndShow('connexion');                      
                });              
            } else {
                console.log('inscription KO');
            };
        })
        .catch((dataInscription) => {
            console.error("Error : ",dataInscription.message)
        });    
});

// On gère la soumission du formulaire de modification
formModification.addEventListener('submit', (event) => {
    // On empeche le comportement de soumission par défaut
    event.preventDefault();
    if (isAvatarPerso) {
        console.log('submit formModification Avatar Perso'); // Pour mon info dans la console
    } else {
        console.log('submit formModification'); // Pour mon info dans la console
    };
    // Une variable pour récupérer les éléments à transmettre au script php        
    let dataModification;
    // On va distinguer le cas où l'utilisateur propose son propre avatar
    if (isAvatarPerso) {
        const avatarPerso = document.getElementsByName('avatarPersoInputFile')[0];        

        // On récupère les éléments à transmettre au script php
        dataModification = new FormData();

        dataModification.append('avatarPerso', avatarPerso.files[0]);
        dataModification.append('loginCurrent', currentProfil.login);
        dataModification.append('loginModification', document.querySelector('#loginModification').value);
        dataModification.append('pwdModification1',calcMD5(document.querySelector('#pwdModification1').value));
        dataModification.append('pwdModification2',calcMD5(document.querySelector('#pwdModification2').value));        
        dataModification.append('nomModification', document.querySelector('#nomModification').value);
        dataModification.append('prenomModification', document.querySelector('#prenomModification').value);
        dataModification.append('avatarActuel',currentProfil.avatar);

        // On va traiter la modification via PHP sur le serveur,
        // Pas de headers pour fetch ici car on passe un objet formData
        fetch("./espaceMembres/modificationAvatarPersoFetch.php", {
            method: 'POST',
            credentials: "include",
            body: dataModification,//JSON.stringify(dataModification),
        })
            .then((response) => response.json())
            .then((dataModification) => {
                if (dataModification.modificationOK) {
                    console.log('Modification avec avatar perso OK');  // Pour mon info dans la console
                    // On crée un objet de la classe Profil pour stocker les éléments utiles à l'affichage
                    currentProfil = new Profil(dataModification.loginModification, dataModification.avatarModification, dataModification.nomModification, dataModification.prenomModification, dataModification.parties, dataModification.statistiques);
                    // On le stocke sur le client                
                    sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
                    msgModificationOK.style.display = 'block';
                    msgModificationOK.innerHTML = "<b>Fichier uploadé avec succès</b>";
                    // On cache la div de feedback de réussite au bout de 3 secondes
                    window.setTimeout(function(){
                        msgModificationOK.style.display = 'none';
                    },3000);                                         
                    msgModificationKO.style.display = 'none';  
                    avatarPerso.value = '';                                  
                    sessionOKProfil.innerHTML = `                
                    <div class="msg">
                        <strong>Super ${currentProfil.prenom} ${currentProfil.nom} !</strong>
                        Ton profil a correctement été modifié.
                    </div>             
                    <div class="row">
                    <div class="col c4 card">
                        <h3>Profil à jour</h3>
                        <ul>
                            <li><b>Avatar</b> : <img class="imgAvatarNav" src="${currentProfil.avatar}"/> </li>
                            <li><b>Prénom</b> : ${currentProfil.prenom}</li>
                            <li><b>Nom</b>    : ${currentProfil.nom}</li>
                            <li><b>Login</b>  : ${currentProfil.login}</li>
                        </ul>										
                    </div>
                    `;
                    btnModificationAbandon.value = 'Fermer';
                    btnLoginInfos.innerHTML = `<img class="imgAvatarNav" src="${currentProfil.avatar}"/> ${currentProfil.prenom} ${currentProfil.nom}`;                    
                } else {
                    msgModificationKO.style.display = 'block';                                
                    msgModificationOK.style.display = 'none';
                    msgModificationKO.innerHTML = `<b>Fichier non uploadé</b><br>${dataModification.msgModificationKO}`;                                                  
                }         
            })
            .catch((responseData) => {
                console.error("Error : ",responseData.message)
            });
    } else {
        // On récupère les éléments à transmettre au script php
        dataModification = {
            loginCurrent: currentProfil.login,
            loginModification: document.querySelector('#loginModification').value,
            pwdModification1: calcMD5(document.querySelector('#pwdModification1').value),
            pwdModification2: calcMD5(document.querySelector('#pwdModification2').value),
            nomModification: document.querySelector('#nomModification').value,
            prenomModification: document.querySelector('#prenomModification').value,
            isAvatarPerso: isAvatarPerso,
            avatarModification: document.querySelector('[name="avatarModification"]:checked').value,
            avatarActuel: currentProfil.avatar,
        };
    
        // On va traiter la modification via PHP sur le serveur
        fetch("./espaceMembres/modificationFetch.php", {
            method: 'POST',
            credentials: "include",
            body: JSON.stringify(dataModification),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        })
            .then((response) => response.json())
            .then((dataModification) => {            
                if (dataModification.msgModificationKO != '') {
                    msgModificationOK.style.display = 'none';
                    msgModificationKO.innerHTML = dataModification.msgModificationKO;                
                    msgModificationKO.style.display = 'block';                                    
                } else {
                    msgModificationKO.style.display = 'none';                    
                    msgModificationOK.innerHTML = dataModification.msgModificationOK;
                    msgModificationOK.style.display = 'block';
                }
                // Si le script PHP renvoie que tout est OK pour la session alors on stocke les infos en local dans le navigateur
                // Et on affiche un message de bienvenue
                if (dataModification.modificationOK) {
                    console.log('Modification OK');  // Pour mon info dans la console
                    // On crée un objet de la classe Profil pour stocker les éléments utiles à l'affichage
                    currentProfil = new Profil(dataModification.loginModification, dataModification.avatarModification, dataModification.nomModification, dataModification.prenomModification, dataModification.parties, dataModification.statistiques);
                    // On le stocke sur le client                
                    sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
                    msgModificationOK.style.display = 'block';
                    // On cache la div de feedback de réussite au bout de 3 secondes
                    window.setTimeout(function(){
                        msgModificationOK.style.display = 'none';
                    },3000);                                         
                    msgModificationKO.style.display = 'none';                
                    sessionOKProfil.innerHTML = `                
                    <div class="msg">
                        <strong>Super ${currentProfil.prenom} ${currentProfil.nom} !</strong>
                        Ton profil a correctement été modifié.
                    </div>             
                    <div class="row">
                    <div class="col c4 card">
                        <h3>Profil à jour</h3>
                        <ul>
                            <li><b>Avatar</b> : <img class="imgAvatarNav" src="${currentProfil.avatar}"/> </li>
                            <li><b>Prénom</b> : ${currentProfil.prenom}</li>
                            <li><b>Nom</b>    : ${currentProfil.nom}</li>
                            <li><b>Login</b>  : ${currentProfil.login}</li>
                        </ul>										
                    </div>
                    `;
                    btnModificationAbandon.value = 'Fermer';
                    btnLoginInfos.innerHTML = `<img class="imgAvatarNav" src="${currentProfil.avatar}"/> ${currentProfil.prenom} ${currentProfil.nom}`;
                    // On réinitialise/met à jour le choix des avatars au cas où on vienne de le modifier                    
                    chooseAvatars();
                } else {
                    console.log('Modification KO');
                    msgModificationKO.style.display = 'block';
                    msgModificationOK.style.display = 'none';                
                };
            })
            .catch((dataModification) => {
                console.error("Error : ",dataModification.message)
            });
    };    
});

// On gère la soumission du formulaire de création d'une partie
formCreateGame.addEventListener('submit', (event) => {
    // On empeche le comportement de soumission par défaut
    event.preventDefault();
    console.log('submit formCreateGame'); // Pour mon info dans la console

    // On récupère les éléments à transmettre au script php
    let dataCreateGame = {
        loginCurrent: currentProfil.login,         
        opponentCreateGame: document.querySelector('#opponentCreateGame').value,
        nameCreateGame: document.querySelector('#nameCreateGame').value
    };

    // On va traiter la connexion via PHP sur le serveur
    fetch("./espaceMembres/createGameFetch.php", {
        method: 'POST',
        credentials: "include",
        body: JSON.stringify(dataCreateGame),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    })
        .then((response) => response.json())
        .then((dataCreateGame) => {
            if (dataCreateGame.msgCreateGameKO != '') {
                msgCreateGameOK.style.display = 'none';
                msgCreateGameKO.innerHTML = dataCreateGame.msgCreateGameKO;
                msgCreateGameKO.style.display = 'block';                                    
            } else {
                msgCreateGameKO.style.display = 'none';                    
                msgCreateGameOK.innerHTML = dataCreateGame.msgCreateGameOK;
                msgCreateGameOK.style.display = 'block';                   
            };            
            // Si le script PHP renvoie que tout est OK pour la session alors on stocke les infos en local dans le navigateur
            // Et on affiche un message de bienvenue            
            if (dataCreateGame.createGameOK) {
                console.log('Création Partie OK'); 
                // Au bout de 2 secondes :
                // - On cache la div de feedback de réussite 
                // - On affiche le tableau des parties mis à jour
                window.setTimeout(function(){
                    msgCreateGameOK.style.display = 'none';
                    // On crée un objet de la classe Profil pour stocker les éléments utiles à l'affichage
                    // Il faut en fait mettre à jour les données sur les parties de l'utilisateur
                    // On ne pourra pas ajouter une ligne directement il faut récrire le tableau via le script php

                    // On va redefinir le profil courant
                    currentProfil = new Profil(currentProfil.login, currentProfil.avatar, currentProfil.nom, currentProfil.prenom, dataCreateGame.parties, dataCreateGame.statistiques);                                    
                    // On le stocke sur le client
                    sessionStorage.setItem('currentProfil',JSON.stringify(currentProfil));
                    sessionOKProfil.innerHTML = currentProfil.bienvenue('Il y a du nouveau ici') + currentProfil.parties;
                    sessionOKProfil.style.display = 'block';
                    divCreateGame.style.display = 'none';
                    // Il faut remettre tous les listeners sur les boutons du tableau des parties
                    addMyListenersGamesTab(currentProfil.login);
                },2000); 
            } else {
                console.log('Création Partie KO');
            };
        })
        .catch((dataCreateGame) => {
            console.error("Error : ",dataCreateGame.message)
        });
});

//======================================================================================
//
// INITIALISATION PRINCIPALE
//
//======================================================================================

// On lance cette methode pour initialiser le DOM 
initAuth();