<?php
/**
 * Des variables et autres binz utiles à la gestion du site
 * 
 * @author Sébastien LOZANO
 */


// ===============================================================================
// GESTION DU SITE 
// ===============================================================================
// Nom du dossier
$folderName = '/tictactoeSQL';
// variable pour réimplanter le site facilement et avoir des url absolues 
// plus nécessaire de changer avant upload sur le serveur !
if ((get_ip()=='127.0.0.1')||(substr(get_ip(),0,7)=='192.168') ) {
  $urlRacineSite = 'http://'.$_SERVER['HTTP_HOST'].$folderName;
} else {
  $urlRacineSite = 'https://'.$_SERVER['HTTP_HOST'].'/tictactoteSQL';
}

// variable de version x.y.z où x est le n° de version publiée; y le nieme ajout de fonctionnalités dans la version x et z la nieme révision
$verLoz = '0.9.21';

// ===============================================================================
// VARIABLES DE CONFIGURATIONS 
// ===============================================================================

$nomSiteOngletNav = 'TICTACTOE';
$metaDescription = 'projet bloc4 DIU - tictactoe PHP/SQL';
$metaKeyWords = 'diu, info, tictactoe, sql, php, LOZANO, Maths';
// Année copyright
$anneeCopyright = '2021-'.date('Y').' All Rights Reserved';

// ===============================================================================
// FUNCTION POUR RECUPERER IP
// ===============================================================================

function get_ip() {
    if($_SERVER) {
        if($_SERVER['HTTP_X_FORWARDED_FOR']) {
            $ipT = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if($_SERVER['HTTP_CLIENT_IP']) {
            $ipT = $_SERVER['HTTP_CLIENT_IP'];
        }  else {
            $ipT = $_SERVER['REMOTE_ADDR'];
        }
    } else {
        if(getenv('HTTP_X_FORWARDED_FOR')) {
            $ipT = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_CLIENT_IP')) {
            $ipT = getenv('HTTP_CLIENT_IP');
        } else {
            $ipT = getenv('REMOTE_ADDR');
        }
    }
    
    return $ipT;
  }; // fin fonction get_ip()
?>