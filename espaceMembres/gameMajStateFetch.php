<?php
//=====================================================================================================================
// GESTION DE LA MISE À JOUR D'UNE GRID DE JEU 
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');
// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataMajGame = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataMajGame->msgMajGameKO = '';
$dataMajGame->msgMajGameOK = '';
$dataMajGame->majGameOK = false;


// On vérifie que des données sont bien soumises par le client
if (isset($dataMajGame)){
    // On récupère l'id unique de la partie
    $idGame = $dataMajGame->idGame;
    // On récupère les éléments utiles aux modifs dans des variables
    $etatPartie = $dataMajGame->currentState;    
    $logPlayer = $dataMajGame->logPlayer;

    // On vérifie que cet identifiant n'est pas vide
    if (issetNotempty($idGame)) {
        // On ouvre une connexion au serveur MySQL 
        $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);
        if (!$connexion) {
            $dataMajGame->msgMajGameKO .= 'pb de connexion';
            $dataMajGame->majGameOK = false;
        } else {
            // On selectionne la base de données pour les requêtes
            mysqli_select_db ($connexion,BDD); 
            // On récupère les données de la partie idGame
            // On parcourt la bdd et on range les éventuelles parties identiques existants dans un tableau
            // On définit la requête
            $sql = 'SELECT count(*) FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$idGame).'"';            
            // Si la requete aboutie on traite sinon message d'erreur
            if ($req = mysqli_query($connexion,$sql)) {
                $dataMajGame->msgGameOK .= "SQL OK !<br>";
                // On récupère les résultats dans un tableau
                $dataPartie = mysqli_fetch_array($req);
                // On libère la mémoire
                mysqli_free_result($req);	
                if ($dataPartie[0]==1) {// Une seule partie trouvée
                    // On définit la requête pour mettre à jour l'état de la partie et le joueur actif
                    // Si la partie est terminée on complète aussi les colonne vainqueur et en_cours
                    $sql = 'SELECT etat_partie,joueur_X,joueur_O FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$idGame).'"';
                    // Si la requete aboutie on traite, sinon msg d'erreur
                    if ($req = mysqli_query($connexion,$sql)) {
                        $datasTmp = mysqli_fetch_array($req);
                        $joueurActif = $dataMajGame->activePlayer == 'X' ? $datasTmp['joueur_X'] : $datasTmp['joueur_O'];
                        // On libère la mémoire
                        mysqli_free_result($req);
                    } else {
                        $dataMajGame->msgMajGameKO .= "Pb avec la recup du joueur actif. <br>";
                    };
                    // On regarde si la partie est terminée
                    $enCours = $dataMajGame->isActiveGame == true ? 1 : 0;
                    $vainqueur = $dataMajGame->winner == null ? null : $dataMajGame->winner;
                    // On met à jour la partie
                    if ($vainqueur == null) {
                        $sqlMaj = '
                        UPDATE diu_parties
                        SET etat_partie = "'.mysqli_escape_string($connexion,$etatPartie).'",
                            joueur_actif = "'.mysqli_escape_string($connexion,$joueurActif).'",                            
                            en_cours = "'.mysqli_escape_string($connexion,$enCours).'"                            
                        WHERE nom_partie = "'.mysqli_escape_string($connexion,$idGame).'"';    
                    } else {
                        $sqlMaj = '
                        UPDATE diu_parties
                        SET etat_partie = "'.mysqli_escape_string($connexion,$etatPartie).'",
                            joueur_actif = "'.mysqli_escape_string($connexion,$joueurActif).'",                            
                            en_cours = "'.mysqli_escape_string($connexion,$enCours).'",
                            vainqueur = "'.mysqli_escape_string($connexion,$vainqueur).'"
                        WHERE nom_partie = "'.mysqli_escape_string($connexion,$idGame).'"';    
                    };                    
                    
                    // Si la requête aboutie on traite sinon message d'erreur
                    if ($reqMaj = mysqli_query($connexion,$sqlMaj)) {
                        // On met à jour les parties du joueur connecté
                        $dataMajGame->parties = "";
                        // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL                            
                        $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$logPlayer).'" OR joueur_O="'.mysqli_escape_string($connexion,$logPlayer).'"';
                        // Si la requête aboutie on traite sinon message d'erreur
                        if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                            // On récupère les résultats dans un tableau
                            $dataParties = mysqli_fetch_array($reqParties);
                            // On libère la mémoire
                            mysqli_free_result($reqParties);
                            // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                            if ($dataParties[0] == 0) {
                                $dataMajGame->parties .= "Pas de parties créées - Ni d'invitations<br>";
                            } else {                                
                                // On redéfinit la requete
                                $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$logPlayer).'" OR joueur_O="'.mysqli_escape_string($connexion,$logPlayer).'"';
                                // Sila requête aboutie on traite sinon message d'erreur
                                if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                    // On crée l'entete du tableau des parties
                                    $dataMajGame->parties.= userHeadTab();									
                                    // On ajoute les lignes une par une
                                    // On va scanner tous les tuples un par un
                                    while ($dataParties = mysqli_fetch_array($reqParties)) {
                                        $dataMajGame->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$logPlayer));
                                    };
                                    // On crée le pied du tableau des parties
                                    $dataMajGame->parties .= userFootTab();
                                } else {
                                    $dataMajGame->parties .= "SQL KO<br>";		
                                };								
                                // On libère la mémoire
                                mysqli_free_result($reqParties);
                            };
                        } else {
                            $dataMajGame->parties .= "SQL KO<br>";
                        };
                        // On libère la mémoire
                        mysqli_free_result($reqMaj);
                    } else {
                        $dataMajGame->msgMajGameKO .= "Pb avec la maj de la partie. <br>";
                    }                   
                } else { // 0 ou plusieurs parties trouvées
                    $dataGame->msgGameKO .= "Pb avec ce nom de partie ! <br>";
                };
            } else {
                $dataGame->msgGameKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
            };
        };
        // On coupe la connexion à la BDD
		mysqli_close($connexion);
    };
};
// On renvoie les données vers l'application
echo json_encode($dataMajGame);
exit();
?>