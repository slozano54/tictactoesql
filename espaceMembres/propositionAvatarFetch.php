<?php
//=====================================================================================================================
// GESTION DE LA PROPOSITION D'AVATARS DANS LE FORMULAIRE DE MODIFICATION DU PROFIL UTILISATEUR
//
//@author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataAvatar = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataAvatar->msgKO = '';
$dataAvatar->msgOK = '';
$dataAvatar->avatarOK = false;

// On vérifie que des données sont bien soumises par le client
if (isset($dataAvatar)){
    // On récupère l'url de l'avatar de l'utilisateur courant dans une variable
    // On nettoie la chaine pour éviter les injections indésirables via la fonction maison cleanFormDatas()    
    $currentAvatar = cleanFormDatas($dataAvatar->currentAvatar);    

    // On vérifie que les données saisies ne sont pas vides
    if (issetNotempty($currentAvatar)) {        
        // On récupère le nom de fichier de l'avatar courant à exclure
        $avatarFilenameToExclude = explode('/',$currentAvatar)[count(explode('/',$currentAvatar))-1];
        // On crée le tableau avec les avatars potentiels pour le changement d'avatar
        $avatarTab = filesAlea("../img/avatars/",[$avatarFilenameToExclude]);
        // On propose deux autres avatars au hasard en remplacement
        $i=2;
        $myTempTab = [];
        foreach ($avatarTab as $avatar) {
            // Ici on en veut que 2 en plus de l'avatar actuel mais pas l'actuel
            if ($i<4) {
                array_push($myTempTab,$avatar);                
                $i++;
            };
        };
        $dataAvatar->avatar2 = str_replace('..','.',$myTempTab[0]);
        $dataAvatar->avatar3 = str_replace('..','.',$myTempTab[1]);
        $dataAvatar->avatarOK = true;
    } else {
        $dataAvatar->avatarOK = false;
        $dataAvatar->msgKO .= "Echec !<br>";
    };
};

// On renvoie les données vers l'application
echo json_encode($dataAvatar);
exit();
?>

