<?php
//=====================================================================================================================
// GESTION DE LA MODIFICATION DU PROFIL UTILISATEUR
//
//@author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header("Access-Control-Allow-Origin: *");


$pathAvatarsPerso = 'img/avatarsPerso/';
// On crée le dossier pour stocker les avatars persos s'il n'existe pas
if (!file_exists("../".$pathAvatarsPerso)) {
    mkdir("../".$pathAvatarsPerso, 0775, true);
};

// Un booléen pour les tests
$isAllOK = true;
// On ajoute quelques éléments pour le retour vers l'application
$msgModificationKO = '';
$msgModificationOK = '';
$modificationOK = false;

$avatarPerso = $_FILES['avatarPerso'];
$loginCurrent = cleanFormDatas($_POST['loginCurrent']);    
$loginModification = cleanFormDatas($_POST['loginModification']);
$pwdModification1 = cleanFormDatas($_POST['pwdModification1']);
$pwdModification2 = cleanFormDatas($_POST['pwdModification2']);
$nomModification = cleanFormDatas($_POST['nomModification']);
$prenomModification = cleanFormDatas($_POST['prenomModification']);
$avatarActuel = cleanFormDatas($_POST['avatarActuel']);    

// Vérifications
// Le login courant A FINIR
if (!issetNotempty($loginCurrent)) {
    $isAllOK = false;
    $msgModificationKO .= 'Le login courrant est absent<br>';
};
// Un petit malin pourrait modifier le login courant !
if ($loginCurrent != $loginModification) {
    $isAllOK = false;
    $msgModificationKO .= 'Hacker ! Le login ne peut pas être modifié<br>';
};
// Mot de passe 1
if (!issetNotempty($pwdModification1) || $pwdModification1 == md5("")) {
    $isAllOK = false;
    $msgModificationKO .= 'Mot de passe manquant<br>';
};
// Mot de passe 2
if (!issetNotempty($pwdModification2) || $pwdModification2 == md5("")) {
    $isAllOK = false;
    $msgModificationKO .= 'Confirmation du mot de passe manquante<br>';
};
// Concordance mots de passe
if ($pwdModification1 != $pwdModification2) {
    $isAllOK = false;
    $msgModificationKO .= 'Les mots de passe sont différents<br>';
};
// Le nom
if (!issetNotempty($nomModification)) {
    $isAllOK = false;
    $msgModificationKO .= 'Nom manquant<br>';
};
// Le prenom
if (!issetNotempty($prenomModification)) {
    $isAllOK = false;
    $msgModificationKO .= 'Prénom manquant<br>';
};
// Le fichier d'avatar perso
// Le poids du fichier d'avatar perso
$avatarSize =$avatarPerso['size'];
// L'extension du fichier d'avatar perso
$extension = strtolower(pathinfo($avatarPerso['name'], PATHINFO_EXTENSION));
$extensionsValides = ['jpg', 'jpeg', 'png'];

if (!isset($avatarPerso)) {    
    $isAllOK = false;
    $msgModificationKO .= 'Pas de fichier d\'avatar proposé<br>';
} else {
    // Directive upload_max_filesize
    if ($avatarSize == 0) {
        $isAllOK = false;
        $msgModificationKO .= 'Directive upload_max_filesize du serveur<br>';
    };    
    // Extension
    if (!in_array($extension, $extensionsValides)) {
        $isAllOK = false;
        $msgModificationKO .= 'Avatar au format jpg ou png uniquement<br>';
    };
    // Poids
    if ( $avatarSize > (2 * 1024 * 1024)) {
        $isAllOK = false;
        $msgModificationKO .= 'Fichier de 2 Mo maximum<br>';
    };
};

// Si tout est OK, on lance la modifica tion du profil dans la BDD
// Les dimensions du fichier d'avatar perso seront de 64 x 64
// Le script va redimensionner tout seul les images 
if ($isAllOK) {
    // Le nouveau nom du fichier horodaté
    $new_name = pathinfo($avatarPerso['name'], PATHINFO_FILENAME) . time() . '.' . $extension;    
    // Le chemin pour l'enregistrement sur le serveur
    $save = "../".$pathAvatarsPerso . $new_name;
    // l'url de stockage dans la BDD
    $avatarModification = "./".$pathAvatarsPerso . $new_name;
    // On a besoin de la variable de session avatar pour le formulaire qui propose des avatars
    $_SESSION['avatar'] = $avatarModification;
    // On déplace le fichier téléchargé sur le serveur
    move_uploaded_file($avatarPerso['tmp_name'], $save); 
    // On récupère des infos
    $info = getimagesize($save);
    // Son type mime
    $mime = $info['mime'];
    // On switch pour utiliser la fonction de création ad hoc
    switch ($mime) {        
        case 'image/jpeg':
            $image_create_func = 'imagecreatefromjpeg';
            $image_save_func = 'imagejpeg';
            break;
        case 'image/png':
            $image_create_func = 'imagecreatefrompng';
            $image_save_func = 'imagepng';
            break;
        default: 
            throw new Exception('Unknown image type.');
    };      
    // On récupère les dimensions
    list($width, $height) = getimagesize($save);
    // Si on n'a pas 64 en largeur ou en hauteur, on redimensionne
    if (!($width==64) || !($height==64)) {
        $modwidth = 64;  // target width
        $diff = $width / $modwidth;
        $modheight = $height / $diff;
        $tn = imagecreatetruecolor($modwidth, $modheight) ;
        $image = $image_create_func($save) ;
        imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ;
        $image_save_func($tn, $save) ;
    };
    // On supprime l'ancien avatar si c'est un avatar perso    
    if ( pathinfo($avatarActuel, PATHINFO_DIRNAME) == "./img/avatarsPerso") {
        unlink("../" . $pathAvatarsPerso . pathinfo($avatarActuel, PATHINFO_FILENAME) . '.' . pathinfo($avatarActuel, PATHINFO_EXTENSION));
    };
    // On affectue le boléen pour le retour à l'application
    $modificationOK = true;
    // On enregistre tout ça dans la BDD
    // TODO
    $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
    if (!$connexion) {
        $modificationOK = false;
        $msgModificationKO .= "Echec connexion BDD<br>";
    } else {
        $modificationOK = true;
        // On selectionne la base de données
        mysqli_select_db ($connexion,BDD);                        
        // on parcourt la bdd et on range les éventuels login identiques existants dans un tableau
        // On définit la requête
        $sql = 'SELECT count(*) FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$loginCurrent).'"';
        // Si la requete aboutie on traite sinon message d'erreur
        if ($req = mysqli_query($connexion,$sql)) {                            
            // On récupère les résultats dans un tableau
            $dataSQL = mysqli_fetch_array($req);                            
            // On libère la mémoire
            mysqli_free_result($req);
            if ($dataSQL[0] == 1) {//si il y a exactement un login identique existe, on modifie la base de données                                
                // On définit la requête
                $sql = '
                UPDATE diu_membres 
                SET login = "'.mysqli_escape_string($connexion,$loginCurrent).'",
                    pass_crypt = "'.mysqli_escape_string($connexion,$pwdModification1).'",
                    nom = "'.mysqli_escape_string($connexion,$nomModification).'",
                    prenom = "'.mysqli_escape_string($connexion,$prenomModification).'",
                    avatar = "'.mysqli_escape_string($connexion,$avatarModification).'"
                WHERE login = "'.mysqli_escape_string($connexion,$loginCurrent).'"';
                // Si la requête aboutie on traite sinon message d'erreur
                if ($reqModification = mysqli_query($connexion,$sql)) {
                    $modificationOK = true;
                    // $msgModificationOK .= "Modification réussie.<br>";                                                                        

                    // On met à jour les parties du joueur courant
                    $parties = "";
                    // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL                            
                    $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                    // Si la requête aboutie on traite sinon message d'erreur
                    if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                        // On récupère les résultats dans un tableau
                        $dataParties = mysqli_fetch_array($reqParties);
                        // On libère la mémoire
                        mysqli_free_result($reqParties);
                        // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                        if ($dataParties[0] == 0) {
                            $parties .= "Pas de parties créées - Ni d'invitations<br>";
                        } else {
                            // On génère le tableau des stats                                            
                            $statistiques = statsUsersTab();                                                                                   
                            // On redéfinit la requete
                            $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                            // Si la requête aboutie on traite sinon message d'erreur
                            if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                // On crée l'entete du tableau des parties
                                $parties.= userHeadTab();									
                                // On ajoute les lignes une par une
                                // On va scanner tous les tuples un par un
                                while ($dataParties = mysqli_fetch_array($reqParties)) {
                                    $parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$loginCurrent));
                                };
                                // On crée le pied du tableau des parties
                                $parties .= userFootTab();
                            } else {
                                $modificationOK = false;
                                $msgModificationKO .= "Erreur SQL !<br>".mysqli_error();
                            };								
                            // On libère la mémoire
                            mysqli_free_result($reqParties);
                        };
                    } else {
                        $modificationOK = false;
                        $msgModificationKO .= "Erreur SQL !<br>".mysqli_error();
                    };
                } else {
                    $modificationOK = false;
                    $msgModificationKO .= "Erreur SQL !<br>".mysqli_error();
                };   
                // On libère la mémoire
                mysqli_free_result($reqModification);
            } else { //sinon on ne modifie pas ce compte
                $modificationOK = false;
                $msgModificationKO .= 'Echec de la modification !<br/> Deux login identiques dans la BDD ?!';
            };
        } else {
            $modificationOK = false;
            $msgModificationKO .= "Erreur SQL !<br>".mysqli_error();
        };
    };
    // On coupe la connexion à la BDD
    mysqli_close($connexion);
} else {
    $modificationOK = false;    
};

// On remplit le tableau de retour
$dataModification = array(        
    'avatarModification'    => $avatarModification,
    'loginCurrent'          => $loginCurrent,
    'loginModification'     => $loginModification,
    'pwdModification1'      => $pwdModification1,
    'pwdModification2'      => $pwdModification2,
    'nomModification'       => $nomModification,  
    'prenomModification'    => $prenomModification,
    'modificationOK'        => $modificationOK,
    'msgModificationKO'     => $msgModificationKO,
    'msgModificationOK'     => $msgModificationOK,
    'parties'               => $parties,
    'statistiques'          => $statistiques,
);
// On renvoie les données vers l'application
echo json_encode($dataModification);
exit();
?>

