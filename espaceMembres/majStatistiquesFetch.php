<?php
//=====================================================================================================================
// GESTION DE LA MISE À JOUR DES STATISTIQUES
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');
// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataMajStatistiques = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataMajStatistiques->msgMajStatistiquesKO = '';
$dataMajStatistiques->msgMajStatistiquesOK = '';
$dataMajStatistiques->majStatistiquesOK = false;


// On vérifie que des données sont bien soumises par le client
if (isset($dataMajStatistiques)){
    $dataMajStatistiques->statistiques = statsUsersTab();
};
// On renvoie les données vers l'application
echo json_encode($dataMajStatistiques);
exit();
?>