<?php
// ===============================================================================
// VARIABLES GLOBALES POUR LA CONNEXION À LA BDD
//
// @author Sébastien LOZANO
// ===============================================================================

/**
 * Remplacer les valeurs suivantes par les valeurs exactes
 * Puis renommer ce fichier connect.php 
*/ 

define ('SERVEUR', "addresseDuServeur");
define ('LOGIN', "identifiantAdminSQL"); 
define ('MDP', "MotDePasseAdminSQL");
define ('BDD', "nomDeLaBaseDeDonnees");
?>
