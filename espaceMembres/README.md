# Fichier de configuration de la connexion à la BDD

- Modifier le fichier **./espaceMembres/connectModel.php** avec les paramètres de connexion à la base de données.
- Renommer ce fichier en **./espaceMembres/connect.php**

# Commandes SQL pour créer la base de données, créer et nourrir ses tables

- Dans **phpMyAdmin** créer manuellement une nouvelle base de données avec **utf8mb4_general_ci** en guise d'interclassement.
Par exemple, on la nomme  **maSuperBaseDeOuf**

- Dans la base **maSuperBaseDeOuf**, on crée une table pour les **membres** avec les colonnes nécessaires via une commande SQL.

```sql
CREATE TABLE diu_membres (
`pk` int NOT NULL AUTO_INCREMENT,
`login` text NOT NULL,
`pass_crypt` text NOT NULL,  
`nom` text NOT NULL,
`prenom` text NOT NULL,
`avatar` text NOT NULL,
PRIMARY KEY (`pk`)
) ENGINE=MyISAM CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;
```
> La colonne **pk** contiendra la clef primaire.
>
> La colonne **login** contiendra le login de l'utilisateur.
> La colonne **login** est soumise à la contrainte **UNIQUE**.
>
> La colonne **pass_crypt** contiendra le mot de passe crypter avec md5().
>
> La colonne **nom** contiendra le nom de l'utilisateur.
>
> La colonne **prenom** contiendra le prénom de l'utilisateur.
>
> La colonne **avatar** contiendra l'url de l'avatar retenu.
>

- On ajoute quelques utilisateurs à la main via une commande SQL pour les tests.

```sql
INSERT INTO `diu_membres` (`pk`, `login`, `pass_crypt`, `nom`, `prenom`, `avatar`) VALUES
(1, 'slozano', MD5('slozano'), 'LOZANO', 'SÃ©bastien', './img/avatars/devil-icon.png'),
(2, 'jcaisse', MD5('jcaisse'), 'CAISSE', 'Jean', './img/avatars/Avengers-Hulk-icon.png'),
(3, 'bturik', MD5('bturik'), 'TURIK', 'Barbie', './img/avatars/devil-love-icon.png'),
(4, 'pochon', MD5('pochon'), 'OCHON', 'Pol', './img/avatars/angry-bird-yellow-icon.png');
```

- Dans la base **maSuperBaseDeOuf**, on crée une table pour les **parties** avec les colonnes nécessaires via une commande SQL.

```sql
CREATE TABLE diu_parties (
`nom_partie` varchar(100) NOT NULL,
`joueur_X` text NOT NULL,
`joueur_O` text NOT NULL,  
`etat_partie` text NOT NULL,
`joueur_actif` text NOT NULL,
`en_cours` tinyint NOT NULL DEFAULT 1,
`vainqueur` varchar(255) DEFAULT NULL,

PRIMARY KEY (`nom_partie`)
) ENGINE=MyISAM CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;
```

> La colonne **nom_partie** contiendra le nom de la partie, ce sera une partie de la clef primaire. Seule sa valeur n'est pas unique dans la table. Le format sera **nomChoisiPourLaPartie_vs_**
>
> La colonne **joueur_X** contiendra le login du joueur qui créera la partie.
>
> La colonne **joueur_O** contiendra le login du joueur selectionner par le joueur qui aura créé la partie.
>
> La colonne **etat_partie** contiendra une chaine de caractères de longueur 9 contenant des "X" , des "O" et des "_"  correspondant à l'état de la partie. Si elle ne contient plus de "_" c'est que la partie est finie !
>
> La colonne **joueur_actif** contient le login du joueur actif, le premier joueur est celui qui crée la partie.
>
> La colonne **en_cours** vaut 0 ou 1, par défaut 1 puisque la partie est en cours jusqu'à ce qu'elle soit finie !
>
> La colonne **vainqueur** contient le login du vainqueur ou "." lorsque la partie est finie ou NULL tant qu'elle ne l'est pas, NULL par défaut.
>
> La **clef primaire** est constituée par la colonne **nom_partie**.

- On ajoute quelques parties dans la table à la main via une commande SQL pour les tests.

```sql
INSERT INTO `diu_parties` (`nom_partie`, `joueur_X`, `joueur_O`, `etat_partie`, `joueur_actif`,`en_cours`,`vainqueur`) VALUES
('slozanoBturik_vs_', 'slozano', 'bturik', 'XOXXOXOO_', 'slozano','0','bturik'),
('bturikSlozano_vs_', 'bturik', 'slozano', 'XXOOOXXOX', 'slozano','0','Match nul.'),
('slozanoJcaisse_vs_', 'slozano', 'jcaisse', '_________', 'slozano','1',NULL),
('slozanoPochon_vs_', 'slozano', 'pochon', 'XXXOO__OX', 'pochon','0','slozano');
```
