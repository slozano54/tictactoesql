<?php
//=====================================================================================================================
// GESTION DE LA DECONNEXION
//
// @author Sébastien LOZANO
//=====================================================================================================================
// On démarre une nouvelle session ou on reprend la session existante
session_start();
// On detruit toutes les variables d'une session
session_unset();
// On détruit la session
session_destroy();
// On redirige le navigateur vers la page d'index
header('Location: ../');
// On s'assure que la suite du code n'est pas exécutée une fois la redirection effectuée
exit();
?>
