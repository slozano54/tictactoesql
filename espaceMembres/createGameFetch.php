<?php
//=====================================================================================================================
// GESTION DE LA CREATION D'UNE PARTIE
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');
// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataCreateGame = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataCreateGame->msgCreateGameKO = '';
$dataCreateGame->msgCreateGameOK = '';
$dataCreateGame->createGameOK = true;

// On vérifie que des données sont bien soumises par le client
if (isset($dataCreateGame)){
    // On récupère les saisies du formulaire dans des variables
    // On nettoie aussi les chaines pour éviter les injections indésirables via la fonction maison cleanFormDatas()
    // On récupère le login courant qui ne peut être modifié
    // On va vérifier qu'un petit malin n'est pas passé outre en supprimant le disabled de l'input    
    $loginCurrent = cleanFormDatas($dataCreateGame->loginCurrent);    
    $nameCreateGame = cleanFormDatas($dataCreateGame->nameCreateGame);
    $opponentCreateGame = cleanFormDatas($dataCreateGame->opponentCreateGame);

    // On vérifie que les données saisies ne sont pas vides
    if (issetNotempty($loginCurrent) && issetNotempty($nameCreateGame) && issetNotempty($opponentCreateGame)) {        
        // On vérifie qu'un adversaire a été choisi dans le select
        // On vérifie aussi qu'un petit malin a changé la value Choisir dans l'inspecteur pour passer outre la limitation ci-dessus
        // On récupère tous les logins des autres utilisateurs
        $users = [];
        foreach (vsList($loginCurrent) as $user) {
            array_push($users,$user->login);
        };
        if ($opponentCreateGame === 'Choisir') {
            $dataCreateGame->createGameOK = false;
            $dataCreateGame->msgCreateGameKO .= "- Il faut choisir un adversaire.<br>";
        } elseif (!in_array($opponentCreateGame,$users)) {
            $dataCreateGame->createGameOK = false;
            $dataCreateGame->msgCreateGameKO .= "- Cet adversaire ne semble pas faire partie des inscrits.<br>";
        };

        // On ajoute _vs_ à la fin du nom choisi pour la partie 
        // afin de faciliter la gestion des listenenrs du tableau des parties
        $nameCreateGame = $nameCreateGame.'_vs_';

        // On vérifie que le nom de la partie contient la chaine _vs_, sensible à la casse !
        // Ceci n'arrivera plus, puisque qu'on ajoute la chaine _vs_ ci-dessus mais je laisse en l'état dans le code car ça ne gène pas.
        // Comme je me sers du nom des parties pour l'ajout des listeners dans le tableau des parties via des id,
        // on ne peut pas utiliser de chiffres car le format des id ne le tolère pas en HTML
        if (!preg_match("/_vs_/",$nameCreateGame)) {
            $dataCreateGame->createGameOK = false;
            $dataCreateGame->msgCreateGameKO .= "- Le nom de la partie doit contenir <b>_vs_</b>. Attention à la casse !<br>";
        } else {            
            preg_match("/([^A-Za-z\s])/",str_replace("_vs_","",$nameCreateGame),$result);
            if (!empty($result)) {
                $dataCreateGame->createGameOK = false;
                $dataCreateGame->msgCreateGameKO .= "- Le nom de la partie ne doit contenir que des lettres.<br>";   
            } 
        };

        // Si tout est OK on va pouvoir ajouter la partie à la BDD
        if ($dataCreateGame->createGameOK == true) {            
            // On ouvre une connexion au serveur MySQL 
            $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
            if (!$connexion) {
                $dataCreateGame->createGameOK = false;
                $dataCreateGame->msgCreateGameKO .= "Echec connexion BDD<br>";
            } else {
                // On selectionne la base de donnée
                mysqli_select_db ($connexion,BDD);                
                // On parcourt la bdd et on range les éventuelles parties identiques existants dans un tableau
                // On définit la requête
                $sql = 'SELECT count(*) FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$nameCreateGame).'"';            
                // Si la requete aboutie on traite sinon message d'erreur
                if ($req = mysqli_query($connexion,$sql)) {                    
                    // On récupère les résultats dans un tableau
                    $dataSQL = mysqli_fetch_array($req);
                     // On libère la mémoire
                     mysqli_free_result($req);
                    //si aucun autre nom_partie identique existe, on crée la nouvelle partie
                    if ($dataSQL[0] == 0) {
                        // On définit la requête de création
                        $sqlCreateGame = 'INSERT INTO diu_parties VALUES (
                            "'.mysqli_escape_string($connexion,$nameCreateGame).'",
                            "'.mysqli_escape_string($connexion,$loginCurrent).'",
                            "'.mysqli_escape_string($connexion,$opponentCreateGame).'",
                            "_________",
                            "'.mysqli_escape_string($connexion,$loginCurrent).'",
                            DEFAULT,
                            DEFAULT
                            )';
                        // Si la requête aboutie on traite sinon message d'erreur
                        if ($reqCreateGame = mysqli_query($connexion,$sqlCreateGame)) {
                            $dataCreateGame->msgCreateGameOK .= "Création partie réussie.<br>";                            
                            $dataCreateGame->createGameOK = true;
                            // On met à jour les parties du joueur courant
                            $dataCreateGame->parties = "";
                            // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL
                            $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                            // Si la requete aboutie on traite sinon message d'erreur
                            if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                                // On récupère les résultats dans un tableau
                                $dataParties = mysqli_fetch_array($reqParties);
                                // On libère la mémoire
                                mysqli_free_result($reqParties);
                                // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                                if ($dataParties[0] == 0) {
                                    $dataCreateGame->parties .= "Pas de parties créées - Ni d'invitations<br>";
                                } else {
                                    // On génère le tableau des stats
                                    $dataCreateGame->statistiques = statsUsersTab();                                                      
                                    // On redéfinit la requete
                                    $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                                    // Si la requête aboutie on traite sinon message d'erreur
                                    if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                        // On crée l'entete du tableau des parties
                                        $dataCreateGame->parties.= userHeadTab();									
                                        // On ajoute les lignes une par une
                                        // On va scanner tous les tuples un par un
                                        while ($dataParties = mysqli_fetch_array($reqParties)) {
                                            $dataCreateGame->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$loginCurrent));
                                        };
                                        // On crée le pied du tableau des parties
                                        $dataCreateGame->parties .= userFootTab();
                                        // On libère la mémoire
                                        mysqli_free_result($reqParties);										
                                    } else {
                                        $dataCreateGame->parties .= "SQL KO<br>";		
                                    };								
                                };
                            } else {
                                $dataCreateGame->parties .= "SQL KO<br>";
                            }
                        } else {
                            $dataCreateGame->msgCreateGameKO .= "Erreur SQL !<br>".mysqli_error();
                        }   
                    } else { //sinon on ne crée pas cette partie
                        $dataCreateGame->createGameOK = false;
                        $dataCreateGame->msgCreateGameKO .= 'Echec de la création !<br/>Une autre partie possede deja ce nom !';
                    }
                } else {
                    $dataCreateGame->msgCreateGameKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
                }
            };
            // On coupe la connexion à la BDD
			mysqli_close($connexion);
        };        
    } else {
        $dataCreateGame->createGameOK = false;
        $dataCreateGame->msgCreateGameKO .= "Echec de la création d'une partie !<br>Au moins un des champs est vide !<br>";
    }; 

};
// On renvoie les données vers l'application
echo json_encode($dataCreateGame);
exit();
?>

