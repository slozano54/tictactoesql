<?php
//=====================================================================================================================
// GESTION DE LA MISE À JOUR D'UNE GRID DE JEU 
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');
// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataMajParties = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataMajParties->msgMajPartiesKO = '';
$dataMajParties->msgMajPartiesOK = '';
$dataMajParties->majPartiesOK = false;


// On vérifie que des données sont bien soumises par le client
if (isset($dataMajParties)){
    // On récupère le login du joueur connecté
    $logPlayer = $dataMajParties->logPlayer;

    // On vérifie que ce login n'est pas vide
    if (issetNotempty($logPlayer)) {
        // On ouvre une connexion au serveur MySQL 
        $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);
        if (!$connexion) {
            $dataMajParties->msgMajPartiesKO .= 'pb de connexion';
            $dataMajParties->majPartiesOK = false;
        } else {
            // On selectionne la base de données pour les requêtes
            mysqli_select_db ($connexion,BDD); 
            // On cherche le joueur logPlayer
            // On parcourt la bdd et on range les éventuels joueurs homonymes dans un tableau
            // On définit la requête
            $sql = 'SELECT count(*) FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$logPlayer).'"';            
            // Si la requete aboutie on traite sinon message d'erreur
            if ($req = mysqli_query($connexion,$sql)) {
                $dataMajParties->msgMajPartiesOK .= "SQL OK !<br>";
                // On récupère les résultats dans un tableau
                $data = mysqli_fetch_array($req);
                // On libère la mémoire
                mysqli_free_result($req);	
                if ($data[0]==1) {// Un seul joueur trouvé                   
                    // On met à jour les parties du joueur courant
                    $dataMajParties->parties = "";
                    // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL                            
                    $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$logPlayer).'" OR joueur_O="'.mysqli_escape_string($connexion,$logPlayer).'"';
                    // Si la requête aboutie on traite sinon message d'erreur
                    if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                        // On récupère les résultats dans un tableau
                        $dataParties = mysqli_fetch_array($reqParties);
                        // On libère la mémoire
                        mysqli_free_result($reqParties);
                        // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                        if ($dataParties[0] == 0) {
                            $dataMajParties->parties .= "Pas de parties créées - Ni d'invitations<br>";
                        } else {                            
                            // On redéfinit la requete
                            $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$logPlayer).'" OR joueur_O="'.mysqli_escape_string($connexion,$logPlayer).'"';
                            // Si la requête aboutie on traite sinon message d'erreur
                            if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                // On crée l'entete du tableau des parties
                                $dataMajParties->parties.= userHeadTab();									
                                // On ajoute les lignes une par une
                                // On va scanner tous les tuples un par un
                                while ($dataParties = mysqli_fetch_array($reqParties)) {
                                    $dataMajParties->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$logPlayer));
                                };
                                // On crée le pied du tableau des parties
                                $dataMajParties->parties .= userFootTab();
                            } else {
                                $dataMajParties->parties .= "SQL KO<br>";		
                            };								
                            // On libère la mémoire
                            mysqli_free_result($reqParties);										
                        };
                    } else {
                        $dataMajParties->parties .= "SQL KO<br>";
                    };                 
                } else { // 0 ou plusieurs players trouvés
                    $dataMajParties->msgMajPartiesKO .= "Pb avec ce login de joueur ! <br>";
                };
            } else {
                $dataMajParties->msgMajPartiesKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
            };
        };
        // On coupe la connexion à la BDD
		mysqli_close($connexion);
    };
};
// On renvoie les données vers l'application
echo json_encode($dataMajParties);
exit();
?>