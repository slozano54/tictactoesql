<?php
//=====================================================================================================================
// GESTION DE LA MODIFICATION DU PROFIL UTILISATEUR
//
//@author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataModification = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataModification->msgModificationKO = '';
$dataModification->msgModificationOK = '';
$dataModification->modificationOK = false;

// On vérifie que des données sont bien soumises par le client
if (isset($dataModification)){
    // On récupère les saisies du formulaire dans des variables
    // On nettoie aussi les chaines pour éviter les injections indésirables via la fonction maison cleanFormDatas()
    // On récupère le login courant qui ne peut être modifié
    // On va vérifier qu'un petit malin n'est pas passé outre en supprimant le disabled de l'input
    $loginCurrent = cleanFormDatas($dataModification->loginCurrent);    
    $loginModification = cleanFormDatas($dataModification->loginModification);
    $pwdModification1 = cleanFormDatas($dataModification->pwdModification1);
    $pwdModification2 = cleanFormDatas($dataModification->pwdModification2);
    $nomModification = cleanFormDatas($dataModification->nomModification);
    $prenomModification = cleanFormDatas($dataModification->prenomModification);
    $avatarModification = cleanFormDatas($dataModification->avatarModification);
    $avatarActuel = cleanFormDatas($dataModification->avatarActuel);

    // On vérifie que les données saisies ne sont pas vides
    if (issetNotempty($loginCurrent) && issetNotempty($loginModification) && issetNotempty($pwdModification1) && issetNotempty($pwdModification2) && issetNotempty($nomModification) && issetNotempty($prenomModification) && issetNotempty($avatarModification)) {        
        // On vérifie que le login de l'utilisateur courant n'a pas été modifié
        if ($loginCurrent != $loginModification) {        
            $dataModification->msgModificationKO .= "Le login ne peut pas être modifié.";         
        } else {
            $dataModification->modificationOK = true;            
            // On vérifie si les deux mots de passe sont saisies
            if ($pwdModification1 == md5("") || $pwdModification2 == md5("")) {
                $dataModification->modificationOK = false;
                $dataModification->msgModificationKO .= "L'un des mots de passe est vide";
            } else {
                $dataModification->modificationOK = true;                
                // On vérifie si les deux mots de passe sont identiques
                if ($pwdModification1 != $pwdModification2) {
                    $dataModification->modificationOK = false;
                    $dataModification->msgModificationKO .= "Les 2 mots de passe sont differents.";   
                } else { //si les deux mots de passe sont identiques, on se connecte à la bdd                 
                    if (!file_exists("../".$avatarModification)) { // si l'avatar n'existe pas
                        $dataModification->modificationOK = false;
                        $dataModification->msgModificationKO .= 'Problème avec l\'avatar sélectionné';
                    } else {
                        $dataModification->modificationOK = true;                    
                        // On ouvre une connexion au serveur MySQL                    
                        $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
                        if (!$connexion) {
                            $dataModification->modificationOK = false;
                            $dataModification->msgModificationKO .= "Echec connexion BDD<br>";
                        } else {
                            // On supprime l'ancien avatar si c'est un avatar perso
                            $pathAvatarsPerso = 'img/avatarsPerso/';    
                            if ( pathinfo($avatarActuel, PATHINFO_DIRNAME) == "./img/avatarsPerso") {
                                unlink("../" . $pathAvatarsPerso . pathinfo($avatarActuel, PATHINFO_FILENAME) . '.' . pathinfo($avatarActuel, PATHINFO_EXTENSION));
                            };                            
                            // On a besoin de la variable de session avatar pour le formulaire qui propose des avatars
                            $_SESSION['avatar'] = $avatarModification;
                            // Tout est OK on traite la modification dans la BDD
                            $dataModification->modificationOK = true;
                            // On selectionne la base de données
                            mysqli_select_db ($connexion,BDD);                        
                            // on parcourt la bdd et on range les éventuels login identiques existants dans un tableau
                            // On définit la requête
                            $sql = 'SELECT count(*) FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                            // Si la requete aboutie on traite sinon message d'erreur
                            if ($req = mysqli_query($connexion,$sql)) {                            
                                // On récupère les résultats dans un tableau
                                $dataSQL = mysqli_fetch_array($req);                            
                                // On libère la mémoire
                                mysqli_free_result($req);
                                if ($dataSQL[0] == 1) {//si il y a exactement un login identique existe, on modifie la base de données                                
                                    // On définit la requête
                                    $sql = '
                                    UPDATE diu_membres 
                                    SET login = "'.mysqli_escape_string($connexion,$loginCurrent).'",
                                        pass_crypt = "'.mysqli_escape_string($connexion,$pwdModification1).'",
                                        nom = "'.mysqli_escape_string($connexion,$nomModification).'",
                                        prenom = "'.mysqli_escape_string($connexion,$prenomModification).'",
                                        avatar = "'.mysqli_escape_string($connexion,$avatarModification).'"
                                    WHERE login = "'.mysqli_escape_string($connexion,$loginCurrent).'"';
                                    // Si la requête aboutie on traite sinon message d'erreur
                                    if ($reqModification = mysqli_query($connexion,$sql)) {
                                        $dataModification->modificationOK = true;
                                        $dataModification->msgModificationOK .= "Modification réussie.<br>";                                                                        
    
                                        // On met à jour les parties du joueur courant
                                        $dataModification->parties = "";
                                        // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL                            
                                        $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                                        // Si la requête aboutie on traite sinon message d'erreur
                                        if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                                            // On récupère les résultats dans un tableau
                                            $dataParties = mysqli_fetch_array($reqParties);
                                            // On libère la mémoire
                                            mysqli_free_result($reqParties);
                                            // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                                            if ($dataParties[0] == 0) {
                                                $dataModification->parties .= "Pas de parties créées - Ni d'invitations<br>";
                                            } else {
                                                // On génère le tableau des stats                                            
                                                $dataModification->statistiques = statsUsersTab();                                                                                   
                                                // On redéfinit la requete
                                                $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                                                // Si la requête aboutie on traite sinon message d'erreur
                                                if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                                    // On crée l'entete du tableau des parties
                                                    $dataModification->parties.= userHeadTab();									
                                                    // On ajoute les lignes une par une
                                                    // On va scanner tous les tuples un par un
                                                    while ($dataParties = mysqli_fetch_array($reqParties)) {
                                                        $dataModification->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$loginCurrent));
                                                    };
                                                    // On crée le pied du tableau des parties
                                                    $dataModification->parties .= userFootTab();
                                                } else {
                                                    $dataModification->parties .= "SQL KO<br>";		
                                                };								
                                                // On libère la mémoire
                                                mysqli_free_result($reqParties);
                                            };
                                        } else {
                                            $dataModification->parties .= "SQL KO<br>";
                                        };
                                    } else {
                                        $dataModification->modificationOK = false;
                                        $dataModification->msgModificationKO .= "Erreur SQL !<br>".mysqli_error();
                                    };   
                                    // On libère la mémoire
                                    mysqli_free_result($reqModification);
                                } else { //sinon on ne modifie pas ce compte
                                    $dataModification->modificationOK = false;
                                    $dataModification->msgModificationKO .= 'Echec de la modification !<br/>Un membre possede deja ce login !';
                                };
                            } else {
                                $dataModification->modificationOK = false;
                                $dataModification->msgModificationKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
                            };
                        };
                        // On coupe la connexion à la BDD
                        mysqli_close($connexion);
                    };
                };
            }; 
        };
    } else {
        $dataModification->modificationOK = false;
        $dataModification->msgModificationKO .= "Echec de la modification !<br>Au moins un des champs est vide !<br>";
    };
};
// On renvoie les données vers l'application
echo json_encode($dataModification);
exit();
?>

