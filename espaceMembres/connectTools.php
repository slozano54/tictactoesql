<?php
//=====================================================================================================================
// HELPERS POUR LA GESTION DES PAGES APRÉS CONNEXION
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

/**
 * Une fonction pour vérifier qu'une variable existe non vide
 * 
 * @param mixed - $var : une variable
 * 
 * @return boolean - $mybool : un booléen
 *    
 * @author Sébastien LOZANO
 */
function issetNotempty($var) {
   $mybool = false;
   (isset($var) && !empty($var)) ? $mybool = true : $mybool = false;
   return $mybool;
};

/**
 *  Une fonction pour valider les données soumises par formulaire
 * 
 * @param string - $datas : le string de données passé via un formulaire HTML
 * 
 * @author Sébastien LOZANO
 * 
 */
function cleanFormDatas($datas){
	$datas = trim($datas); // On supprime les espaces en deébut/fin de chaine
	$datas = stripslashes($datas); // On supprime les antislashs
	$datas = htmlspecialchars($datas); // On convertit les caractères spéciaux en entités HTML
	return $datas;
};

/**
 * Une fonction pour tirer au hasard les 3 avatars proposés à l'inscription
 * 
 * @param string - $path : le chemin vers le dossier
 * 
 * @param array - $forbidFiles : les noms des fichiers à exclure dans un tableau
 * 
 * @author Sébastien LOZANO
 */

function filesAlea($path,$forbidFiles = []) {   
   // On crée le tableau avec les fichiers à exclure
   array_push($forbidFiles,".","..");
   // On récupète le contenu du dossier contenant les avatars dans un tableau
   $scandir = array_diff(scandir($path), $forbidFiles);
   // On sélectionne 3 clefs
   $rand_keys = array_rand($scandir, 3);   
   // On renvoie un tableau avec les 3 urls
   $output = [$path.$scandir[$rand_keys[0]],$path.$scandir[$rand_keys[1]],$path.$scandir[$rand_keys[2]]];
   return $output;
}

/**
 * Une fonction pour afficher un espace au lieu d'un underscore
 * 
 * @param string - $char : un caratère
 * 
 * @author Sébastien LOZANO 
 */
function underscoreToSpace($char) {
   return $char == "_" ? " " : $char;
};

/**
 *  Une fonction pour transformer un chaine d'état de partie en tableau
 * 
 * @param string - $str : la chaine corresponndant à l'état de la partie
 * 
 * @return string - $sortie : une chaine avec le code HTML d'un tableau pour afficher l'état de la partie
 * 
 * @author Sébastien LOZANO
 */

 function strToTabPart($str) {        
    $sortie = "
    <table class = \"etatPartie\">
      <tr>
         <td>".underscoreToSpace($str[0])."</td>
         <td>".underscoreToSpace($str[1])."</td>
         <td>".underscoreToSpace($str[2])."</td>
      </tr>
      <tr>
         <td>".underscoreToSpace($str[3])."</td>
         <td>".underscoreToSpace($str[4])."</td>
         <td>".underscoreToSpace($str[5])."</td>
      </tr>
      <tr>
         <td>".underscoreToSpace($str[6])."</td>
         <td>".underscoreToSpace($str[7])."</td>
         <td>".underscoreToSpace($str[8])."</td>
      </tr>
    </table>
    ";
  
    return $sortie;
 }


/**
 * Une fonction pour ouvrir et écrire l'entête du tableau des parties terminée et en cours
 * 
 * @author Sébastien LOZANO
 */

function userHeadTab() {
   return "
   <h4>Parties créées ou auxquelles tu as été invité.</h4>
   <table class=\"table\">
   <thead>
      <tr>									            
            <th>Nom de la partie</th>
            <th>Adversaire</th>
            <th>Etat</th>
            <th>Joueur actif</th>
            <th>Vainqueur</th>
            <th>Actions</th>
      </tr>
   </thead>
   <tbody>
   ";
};

/**
 * Une fonction pour afficher les lignes des parties d'un joueur
 * 
 * @param  array - $dataParties : Contient toutes les infos des parties d'un joueur issue de la table parties de la BDD
 * 
 * @param string - $loginConnexion : Contient le login du joueur connecté
 *  
 * @author Sébastien LOZANO
 */

function userGamesLine($dataParties,$loginConnexion) {
   $sortie = ""; 
   // On ouvre une connexion au serveur MySQL 
   $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);
   if (!$connexion) {
      $sortie .= "
      <tr>
         <td>Pb connexion</td>
         <td>Pb connexion</td>
         <td>Pb connexion</td>
         <td>Pb connexion</td>
         <td>Pb connexion</td>
         <td><a class=\"btn btn-b btn-sm\">Pb connexion</a></td>
      </tr>
   ";
   } else {
      // On selectionne la base de données pour les requêtes
      mysqli_select_db ($connexion,BDD); 
      // On récupère les données du joueur_X
      // On définit la requête 
      $sqlX = 'SELECT * FROM diu_membres WHERE login="'.$dataParties['joueur_X'].'"';      
      // On execute la requête
      $reqX = mysqli_query($connexion,$sqlX);
      // On récupère les résultats dans un tableau
      $dataX = mysqli_fetch_array($reqX);
      // On libère la mémoire
      mysqli_free_result($reqX);			
      // On récupère les données du joueur_O
      // On définit la requête 
      $sqlO = 'SELECT * FROM diu_membres WHERE login="'.$dataParties['joueur_O'].'"';      
      // On execute la requête
      $reqO = mysqli_query($connexion,$sqlO);
      // On récupère les résultats dans un tableau
      $dataO = mysqli_fetch_array($reqO);
      // On libère la mémoire
      mysqli_free_result($reqO);
      // On récupère les données du joueur_actif
      // On définit la requête 
      $sqlActif = 'SELECT * FROM diu_membres WHERE login="'.$dataParties['joueur_actif'].'"';      
      // On execute la requête
      $reqActif = mysqli_query($connexion,$sqlActif);
      // On récupère les résultats dans un tableau
      $dataActif = mysqli_fetch_array($reqActif);
      // On libère la mémoire
      mysqli_free_result($reqActif);
      //===============================================================
      // On affecte toutes les variables qui seront affichées		
      //===============================================================
      // On vérifie si il y a un vainqueur 
      if ($dataParties['vainqueur'] == NULL) {
         $colVainqueur = 'Partie en cours.';
      } elseif ($dataParties['vainqueur'] == 'Match nul.') {
         $colVainqueur = 'Match nul.';
      } else { 
         // On récupère les données du vainqueur
         // On définit la requête
         $sqlVainqueur = 'SELECT * FROM diu_membres WHERE login="'.$dataParties['vainqueur'].'"';      
         // On execute la requête
         $reqVainqueur = mysqli_query($connexion,$sqlVainqueur);
         // On récupère les résultats dans un tableau
         $dataVainqueur = mysqli_fetch_array($reqVainqueur);
         // On libère la mémoire
         mysqli_free_result($reqVainqueur);
         $colVainqueur = $dataVainqueur['prenom'].' '.$dataVainqueur['nom'];
      };
      // On coupe la connexion à la BDD, on n'en a plus besoin
      mysqli_close($connexion);	
      $nomPartie = $dataParties['nom_partie'];
      // On enlève _vs_ du nom de la partie pour l'affichage, qui ne sert qu'à l'ajout des listeners sur les boutons du tableau desparties
      $nomPartieAffichage = substr($nomPartie,0,-4);      
      // La partie est-elle encours ?
      // Le bouton d'action n'aura pas toujours le même icone
      $dataParties['en_cours'] == 0 ? $btnAction = "&#128065;" : $btnAction = "&#9654;";
      if ($dataParties['en_cours'] == 0) {
         $dataAction = "Affichage";
      } else {
         if ($dataParties['etat_partie'] == "_________") {
            $dataAction = "Demarrage";
         } else {
            $dataAction = "Reprise";
         };  
      };      
      // On teste pour savoir qui est l'adversaire et qui est le joueur ayant créé la partie      
      $dataParties['joueur_X'] == $loginConnexion ? $adversaire = $dataO['prenom'].' '.$dataO['nom'] : $adversaire = $dataX['prenom'].' '.$dataX['nom'];        
      // Idem pour les avatars
      $dataParties['joueur_X'] == $loginConnexion ? $avatar = $dataO['avatar'] : $avatar = $dataX['avatar'];        
      // On transforme la chaine d'état de la partie en tableau pour l'affichage
      $dataEtat = $dataParties['etat_partie'];
      $etat = strToTabPart($dataEtat);
      $joueurActif = $dataActif['prenom'].' '.$dataActif['nom'];      
      // On affecte la variable de sortie
      $sortie .= "
            <tr>
               <td>$nomPartieAffichage</td>
               <td><img class=\"imgTabParties\" src=\"$avatar\"/><br>$adversaire</td>
               <td>$etat</td>
               <td>$joueurActif</td>
               <td>$colVainqueur</td>
               <td><a id=\"$nomPartie\" class=\"btn btn-b btn-sm\" data-action=\"$dataAction\">$btnAction</a> <a id=\"suppr$nomPartie\" class=\"btn btn-c btn-sm\">&#x1F5D1;</a></td>
            </tr>
         ";
   };

   return $sortie;
}

 /**
 * Une fonction pour fermer le tableau des parties en cours
 * 
 * @author Sébastien LOZANO
 */
function userFootTab() {
   return "
      </tbody>
      <tfoot>
         <tr>									            
            <th>Nom de la partie</th>
            <th>Adversaire</th>
            <th>Etat</th>
            <th>Joueur actif</th>
            <th>Vainqueur</th>
            <th>Actions</th>
         </tr>
      </tfoot>
   </table>
   ";
};

 /**
  * Une fonction pour récupérer les membres inscrits sauf l'utilisateur connecté
  *
  * @param string - $loggedUser : le login du joueur connecté
  *   
  * @return array - Un tableau avec tous les utilisateurs de la BDD sauf le joueur courant
  *
  * @author Sébastien LOZANO  
  */

function vsList($loggedUser) {
   $sortie=[];
   // On ouvre une connexion au serveur MySQL
   $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
   if (!$connexion) {
      $sortie=['pb de connexion'];
   } else {
      // On selectionne la base de données
      mysqli_select_db ($connexion,BDD); 
      // On définit la requête pour récupérer tous les joueurs sauf le joueur connecté
      $sql = 'SELECT login, nom, prenom FROM diu_membres WHERE login<>"'.$loggedUser.'"';      
      // On execute la requete
      $req = mysqli_query($connexion,$sql);
      // On récupère toutes les lignes dans un tableau php sous forme d'objet      
      while ($data = mysqli_fetch_array($req)) {
         // array_push($sortie,$data['login']);
         $tmpObj = new stdClass();
         $tmpObj->login = $data['login'];
         $tmpObj->firstLastName = $data['prenom']." ".$data['nom'];
         array_push($sortie,$tmpObj);
      };      
      // On libère la mémoire
      mysqli_free_result($req);      
   }
   // On coupe la connexion à la BDD, on n'en a plus besoin
   mysqli_close($connexion);	

   return $sortie;
};

/**
 * Une fonction écrire la ligne des statistiques d'un utilisateur
 * 
 * @author Sébastien LOZANO
 */

function statsUserLine($user,$prenom,$nom,$avatar) {
   // On initialise les compteurs.
   $victoires = 0;
   $defaites = 0;
   $nuls = 0;
   $enCours = 0;
   // On ouvre une connexion au serveur MySQL
   $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
   if (!$connexion) {
      $sortie=['pb de connexion'];
   } else {
      // On selectionne la base de données
      mysqli_select_db ($connexion,BDD); 
      // On définit la requête pour récupérer tous les joueurs sauf le joueur connecté
      $sql = 'SELECT * FROM diu_parties WHERE joueur_X="'.$user.'" OR joueur_O="'.$user.'"';      
      // On execute la requete
      $req = mysqli_query($connexion,$sql);
      // On incrémente les variables utiles
      while ($data = mysqli_fetch_array($req)) {
         // On traite les parties finies
         if ($data['en_cours'] == 0) {
            if ($data['vainqueur'] == $user) {
               $victoires++;
            };
            if ($data['vainqueur'] != $user && $data['vainqueur'] != 'Match nul.') {
               $defaites++;
            };
            if ($data['vainqueur'] == 'Match nul.') {
               $nuls++;
            };
         };
         // On traite les parties en cours
         if ($data['en_cours'] == 1) {
            $enCours++;
         };
      };      
      // On libère la mémoire
      mysqli_free_result($req);      
      $total = $victoires + $defaites + $nuls + $enCours;
   }
   // On coupe la connexion à la BDD, on n'en a plus besoin
   mysqli_close($connexion);
   // On transforme en pourcentages
   if ($total != 0) {
      $victoiresPourcent = round($victoires/$total * 100);	
      $defaitesPourcent = round($defaites/$total * 100);
      $nulsPourcent = round($nuls/$total * 100);
      $enCoursPourcent = round($enCours/$total * 100);   
      $totalPourcent = '100';
   } else {
      $victoiresPourcent = '...';
      $defaitesPourcent = '...';
      $nulsPourcent = '...';
      $enCoursPourcent = '...';
      $totalPourcent = '...';
   };

   $sortie ="
   <tr>
      <td rowspan=\"2\"><img class=\"imgTabParties\" src=\"$avatar\"/></td>
      <td rowspan=\"2\">$prenom $nom</td>
      <td scope=\"row\">$victoires</td>
      <td>$defaites</td>
      <td>$nuls</td>
      <td>$enCours</td>
      <td>$total</td>
   </tr>
   <tr>
      <td scope=\"row\">$victoiresPourcent %</td>
      <td>$defaitesPourcent %</td>
      <td>$nulsPourcent %</td>
      <td>$enCoursPourcent %</td>
      <td>$totalPourcent %</td>
   </tr>
   ";

   return $sortie;
};

/**
 * Une fonction pour créer le tableau des statistiques du jeu pour chaque utilisateur
 * 
 * @author Sébastien LOZANO
 */
function statsUsersTab() {
   // On prépare l'entête du tableau
   $sortie = "
   <table class=\"tableStats\">
   <thead>
      <tr>									            
         <th>Avatar</th>
         <th>Membres</th>
         <th>Victoires</th>
         <th>Défaites</th>
         <th>Matchs nuls</th>
         <th>En cours</th>
         <th>Total</th>
      </tr>
   </thead>
   <tbody>";
   // On ajoute les lignes
   // On ouvre une connexion au serveur MySQL
   $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
   if (!$connexion) {
      $sortie=['pb de connexion'];
   } else {
      // On selectionne la base de données
      mysqli_select_db ($connexion,BDD); 
      // On définit la requête pour récupérer tous les joueurs sauf le joueur connecté
      $sql = 'SELECT login,nom,prenom,avatar FROM diu_membres';      
      // On execute la requete
      $req = mysqli_query($connexion,$sql);
      // Pour chaque login, on ajoute la ligne des stats du joueur
      while ($data = mysqli_fetch_array($req)) {
         $sortie .= statsUserLine($data['login'],$data['prenom'],$data['nom'],$data['avatar']);
      };      
      // On libère la mémoire
      mysqli_free_result($req);      
   }
   // On coupe la connexion à la BDD, on n'en a plus besoin
   mysqli_close($connexion);	  
   // On ferme le tableau
   $sortie .= "
   </tbody>
   <tfoot>
   <tr>
      <th>Avatar</th>									            
      <th>Membres</th>
      <th>Victoires</th>
      <th>Défaites</th>
      <th>Matchs nuls</th>
      <th>En cours</th>
      <th>Total</th>
   </tr>
   </tfoot>
   </table>
   ";

   return $sortie;
};

?>