<?php
//=====================================================================================================================
// GESTION DE LA CONNEXION À L'ESPACE MEMBRES
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$data = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$data->msgConnexionKO = '';
$data->msgConnexionOK = '';
$data->sessionOK = false;

// On vérifie que des données sont bien soumises par le client
if (isset($data)){
	// On récupère les saisies du formulaire dans des variables
	$loginConnexion = cleanFormDatas($data->loginConnexion);
	$pwdConnexion = cleanFormDatas($data->pwdConnexion);	
	// On vérifie que ces données necessaires ne sont pas vides
	if (issetNotempty($loginConnexion) && issetNotempty($pwdConnexion)) {
		// On ouvre une connexion au serveur MySQL 
		$connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
		if (!$connexion) {
			$data->msgConnexionKO .= "Connexion mysql KO<br>"; 			
		} else {
			$data->msgConnexionOK .= "Connexion mysql OK<br>"; 
			// On selectionne la base de donnée
			mysqli_select_db ($connexion,BDD); 
			// On parcourt la bdd pour chercher l'existence du login mot et du mot de passe saisis par l'internaute 
			// On définit la requête
			$sql = 'SELECT count(*) FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$loginConnexion).'" AND pass_crypt="'.mysqli_escape_string($connexion,$pwdConnexion).'"';
			// Si la requete aboutie on traite sinon message d'erreur
			if ($req = mysqli_query($connexion,$sql)) {
				$data->msgConnexionOK .= "SQL OK !<br>";
				// On récupère les résultats dans un tableau
				$dataSQL = mysqli_fetch_array($req);
				// On libère la mémoire
				mysqli_free_result($req);				
				// Si on obtient une réponse, alors l'utilisateur est un membre
				// On ouvre une session pour cet utilisateur et on le connecte à l'espace membre
				if ($dataSQL[0] == 1){
					$data->sessionOK = true;
					$data->msgConnexionOK .= 'Membre OK! login + pwd<br>';
					// On démarre une nouvelle session
					session_start();
					// On définit les variables de session pour PHP
					$_SESSION['login'] = $loginConnexion;
					// On va récupérer toutes les infos					
					$data->msgConnexionOK .= "Connexion mysql OK<br>"; 									
					// Pas la peine de se reconnecter à la bdd tant qu'on n'a pas coupé la connexion
					// On définit la requête
					$sqlMembre = 'SELECT * FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$loginConnexion).'"'; 						
					// Si la requête aboutie on traite sinon erreur
					if ($reqMembre = mysqli_query($connexion,$sqlMembre)) {
						$data->msgConnexionOK .= 'SQL Membre OK!<br>'.$sqlMembre.'<br>';
						// On récupère les résultats dans un tableau
						$dataMembre = mysqli_fetch_array($reqMembre);
						// On libère la mémoire
						mysqli_free_result($reqMembre);
						// On affecte les variable de session et ce qu'il faut dans le json de retour pour l'affichage client							
						$_SESSION['nom'] = $dataMembre['nom'];
						$data->nom = $dataMembre['nom'];
						$_SESSION['prenom'] = $dataMembre['prenom'];
						$data->prenom = $dataMembre['prenom'];
						$_SESSION['avatar'] = $dataMembre['avatar'];							
						$data->avatar = $dataMembre['avatar'];
						$data->parties = "";
						// On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL
						// On définit la requête
						$sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginConnexion).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginConnexion).'"';
						// si la requête aboutie on traite, sinon erreur
						if ($reqParties = mysqli_query($connexion,$sqlParties)) {
							// On récupère les résultats dans un tableau
							$dataParties = mysqli_fetch_array($reqParties);
							// On libère la mémoire
							mysqli_free_result($reqParties);							
							if ($dataParties[0] == 0) { // S'il n'y a aucune ligne, l'utilisateur n'a pas créé de parties et n'a pas été invité
								$data->parties .= "Pas de parties créées - Ni d'invitations<br>";
							} else {
								// On génère le tableau des statistiques								
								$data->statistiques = statsUsersTab();									
								// On redéfinit la requete
								$sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginConnexion).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginConnexion).'"';
								// Si la requête aboutie on traite sinon erreur
								if ($reqParties = mysqli_query($connexion,$sqlParties)) {
									// On crée l'entete du tableau des parties
									$data->parties.= userHeadTab();									
									// On ajoute les lignes une par une
									// On va scanner tous les tuples un par un
									while ($dataParties = mysqli_fetch_array($reqParties)) {
										$data->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$loginConnexion));
									};
									// On crée le pied du tableau des parties
									$data->parties .= userFootTab();
									// On libère la mémoire
									mysqli_free_result($reqParties);										
								} else {
									$data->parties .= "SQL KO<br>";		
								};								
							};
						} else {
							$data->parties .= "SQL KO<br>";
						}							
					} else {
						$data->msgConnexionKO .= 'SQL KO!<br>'.$sqlMembre.'<br>'.mysqli_error();
					}
				} elseif ($dataSQL[0] == 0) {//si le visiteur a saisi un mauvais login ou mot de passe, on ne trouve aucune réponse
					// $data->msgConnexionKO .= 'Membre KO!<br>';
					$data->msgConnexionKO .= 'Login ou mot de passe non reconnu !';
				} else { // sinon, il existe un problème dans la base de données
					$data->msgConnexionKO = 'Plusieurs membres ont<br/>les memes login et mot de passe !';
				}
			} else {
				$data->msgConnexionKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
			};						
		};
		// On coupe la connexion à la BDD
		mysqli_close($connexion);
	} else {
		$data->msgConnexionKO .= "Errreur de saisie !<br>Au moins un des champs est vide !";
	}
}
// On renvoie les données vers l'application
echo json_encode($data);
exit();
?>

