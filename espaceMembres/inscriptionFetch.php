<?php
//=====================================================================================================================
// GESTION DE L'INSCRIPTION D'UN NOUVEAU MEMBRE
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichiers d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataInscription = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataInscription->msgInscriptionKO = '';
$dataInscription->msgInscriptionOK = '';
$dataInscription->inscriptionOK = false;

// On vérifie que des données sont bien soumises par le client
if (isset($dataInscription)){
   // On récupère les saisies du formulaire dans des variables
   // On nettoie aussi les chaines pour éviter les injections indésirables via la fonction maison cleanFormDatas()
   $loginInscription = cleanFormDatas($dataInscription->loginInscription);
   $pwdInscription1 = cleanFormDatas($dataInscription->pwdInscription1);
   $pwdInscription2 = cleanFormDatas($dataInscription->pwdInscription2);
   $nomInscription = cleanFormDatas($dataInscription->nomInscription);
   $prenomInscription = cleanFormDatas($dataInscription->prenomInscription);
   $avatarInscription = cleanFormDatas($dataInscription->avatarInscription);

   // On vérifie que les données saisies ne sont pas vides
   if (issetNotempty($loginInscription) && issetNotempty($pwdInscription1) && issetNotempty($pwdInscription2) && issetNotempty($nomInscription) && issetNotempty($prenomInscription) && issetNotempty($avatarInscription)) {      
      if ($pwdInscription1 != $pwdInscription2) { // si les deux mots de passe sont différents
         $dataInscription->inscriptionOK = false;
         $dataInscription->msgInscriptionKO .= 'Les 2 mots de passe sont differents.';         
      } else {	//si les deux mots de passe sont identiques
         $dataInscription->inscriptionOK = true;
         if (!file_exists("../".$avatarInscription)) { // si l'avatar n'existe pas
            $dataInscription->inscriptionOK = false;
            $dataInscription->msgInscriptionKO .= 'Problème avec l\'avatar sélectionné';
         } else {
            $dataInscription->inscriptionOK = true;
            // On ouvre une connexion au serveur MySQL
            $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
            if (!$connexion) {
               $dataInscription->inscriptionOK = false;
               $dataInscription->msgInscriptionKO .= "Echec connexion BDD<br>";
            } else {
               $dataInscription->inscriptionOK = true;
               // On selectionne la base de données
               mysqli_select_db ($connexion,BDD);            
               // on parcourt la bdd et on range les éventuels login identiques existants dans un tableau
               // On définit la requête
               $sql = 'SELECT count(*) FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$loginInscription).'"';            
               // Si la requete aboutie on traite sinon message d'erreur
               if ($req = mysqli_query($connexion,$sql)) {               
                  // On récupère les résultats dans un tableau
                  $dataSQL = mysqli_fetch_array($req);
                  // On libère la mémoire
                  mysqli_free_result($req);
                  //si aucun autre login identique existe, on inscrit ce nouveau login
                  if ($dataSQL[0] == 0) {
                     // On définit la requête d'insertion
                     $sql = 'INSERT INTO diu_membres VALUES (
                        DEFAULT,
                        "'.mysqli_escape_string($connexion,$loginInscription).'",
                        "'.mysqli_escape_string($connexion,$pwdInscription1).'",
                        "'.mysqli_escape_string($connexion,$nomInscription).'",
                        "'.mysqli_escape_string($connexion,$prenomInscription).'",
                        "'.mysqli_escape_string($connexion,$avatarInscription).'"
                        )';
                     // Si la requête aboutie on traite, sinon message d'erreur
                     if ($reqInscription = mysqli_query($connexion,$sql)) {
                        $dataInscription->inscriptionOK = true;
                        $dataInscription->msgInscriptionOK .= "Inscription réussie.<br>";
                        $dataInscription->msgInscriptionOK .= "Accès au formulaire de <a href='#' class='btn btn-b btn-sm' id='showConnexion2'>connexion</a>";                        
                     } else {
                        $dataInscription->inscriptionOK = false;
                        $dataInscription->msgInscriptionKO .= "Erreur SQL !<br>".mysqli_error();
                     }
                     // On libère la mémoire
                     mysqli_free_result($reqInscription);   
                  } else { //sinon on n'inscrit pas ce login
                     $dataInscription->inscriptionOK = false;
                     $dataInscription->msgInscriptionKO .= 'Echec de l\'inscription !<br/>Un membre possede deja ce login !';
                  }
               } else {
                  $dataInscription->inscriptionOK = false;
                  $data->msgInscriptionKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
               }
            };
            // On coupe la connexion à la BDD
            mysqli_close($connexion);
         }

      }
   } else {
      $dataInscription->inscriptionOK = false;
      $dataInscription->msgInscriptionKO .= "Echec de l'inscription !<br>Au moins un des champs est vide !<br>";
   };
};
// On renvoie les données vers l'application
echo json_encode($dataInscription);
exit();
?>