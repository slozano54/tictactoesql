<?php
//=====================================================================================================================
// GESTION DE LA SUPPRESSION D'UNE PARTIE
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');

// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataDeleteGame = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataDeleteGame->msgDeleteGameKO = '';
$dataDeleteGame->msgDeleteGameOK = '';
$dataDeleteGame->deleteGameOK = false;

// On vérifie que des données sont bien soumises par le client
if (isset($dataDeleteGame)){
    // On récupère l'id de la partie à supprimer    
    $idPartie = $dataDeleteGame->idPartie;
    // On récupère le login courant qui ne peut être modifié
    $loginCurrent = $dataDeleteGame->loginCurrent;

    // On vérifie que les données saisies ne sont pas vides
    if (issetNotempty($idPartie)) {        
        $dataDeleteGame->deleteGameOK = true;
        // Si tout est OK on va pouvoir supprimer la partie à la BDD
        if ($dataDeleteGame->deleteGameOK == true) {
            $dataDeleteGame->msgDeleteGameOK .= "- On peut supprimer la partie à la BDD.<br>";
            // On ouvre une connexion au serveur MySQL 
            $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);    
            if (!$connexion) {
                $dataDeleteGame->deleteGameOK = false;
                $dataDeleteGame->msgDeleteGameKO .= "Echec connexion BDD<br>";
            } else {
                // On sélectionne la base de données
                mysqli_select_db ($connexion,BDD);
                $dataDeleteGame->msgDeleteGameOK .= "Connexion BDD reussie<br>"; 
                // on parcourt la bdd et on range les éventuels login identiques existants dans un tableau
                // On définit la requête
                $sql = 'SELECT count(*) FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$idPartie).'"';            
                // Si la requete aboutie on traite sinon message d'erreur
                if ($req = mysqli_query($connexion,$sql)) {
                    $dataDeleteGame->msgDeleteGameOK .= "SQL OK !<br>";
                    // On récupère les résultats dans un tableau
                    $dataSQL = mysqli_fetch_array($req);
                    //si un nom_partie identique existe, on crée la nouvelle partie
                    if ($dataSQL[0] == 1) {
                        $sqlDeleteGame = 'DELETE FROM diu_parties WHERE nom_partie = "'.mysqli_escape_string($connexion,$idPartie).'"';
                        if ($reqDeleteGame = mysqli_query($connexion,$sqlDeleteGame)) {
                            $dataDeleteGame->msgDeleteGameOK .= "Suppression partie réussie.<br>";                            
                            $dataDeleteGame->deleteGameOK = true;
                            // On libère la mémoire
                            mysqli_free_result($reqDeleteGame);
                            // On met à jour les parties du joueur courant
                            $dataDeleteGame->parties = "";
                            // On récupère la liste des parties de l'utilisateur dans une chaine de caractères via une requete SQL                            
                            $sqlParties = 'SELECT count(*) FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                            // Si la requête aboutie on traite sinon message d'erreur
                            if ($reqParties = mysqli_query($connexion,$sqlParties)) {                            
                                // On récupère les résultats dans un tableau
                                $dataParties = mysqli_fetch_array($reqParties);
                                // On libère la mémoire
                                mysqli_free_result($reqParties);
                                // S'il n'y a aucune ligne, l'utilisateur n'a pâs créé de parties et n'a pas été invité
                                if ($dataParties[0] == 0) {
                                    $dataDeleteGame->parties .= "Pas de parties créées - Ni d'invitations<br>";
                                } else {
                                    // On génère le tableau des stats								    
                                    $dataDeleteGame->statistiques = statsUsersTab();                                                                       
                                    // On redéfinit la requete
                                    $sqlParties = 'SELECT * FROM diu_parties WHERE joueur_X="'.mysqli_escape_string($connexion,$loginCurrent).'" OR joueur_O="'.mysqli_escape_string($connexion,$loginCurrent).'"';
                                    // Si la requête aboutie on traite sinon message d'erreur
                                    if ($reqParties = mysqli_query($connexion,$sqlParties)) {
                                        // On crée l'entete du tableau des parties
                                        $dataDeleteGame->parties.= userHeadTab();									
                                        // On ajoute les lignes une par une
                                        // On va scanner tous les tuples un par un
                                        while ($dataParties = mysqli_fetch_array($reqParties)) {
                                            $dataDeleteGame->parties .= userGamesLine($dataParties,mysqli_escape_string($connexion,$loginCurrent));
                                        };
                                        // On crée le pied du tableau des parties
                                        $dataDeleteGame->parties .= userFootTab();
                                    } else {
                                        $dataDeleteGame->parties .= "SQL KO<br>";		
                                    };								
                                    // On libère la mémoire
                                    mysqli_free_result($reqParties);										
                                };
                            } else {
                                $dataDeleteGame->parties .= "SQL KO<br>";
                            };
                        } else {
                            $dataDeleteGame->msgDeleteGameKO .= "Erreur SQL !<br>".mysqli_error();
                        }   
                    } else { //sinon on ne supprime pas cette partie
                        $dataDeleteGame->deleteGameOK = false;
                        $dataDeleteGame->msgDeleteGameKO .= 'Echec de la suppression !<br/>La partie n\'existe pas !';
                    }
                } else {
                    $dataDeleteGame->msgDeleteGameKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
                }
            };
            // On coupe la connexion à la BDD
			mysqli_close($connexion);
        };        
    } else {
        $dataDeleteGame->deleteGameOK = false;
        $dataDeleteGame->msgDeleteGameKO .= "Echec de la suppression d'une partie !<br>l'id est vide ou non défini !<br>";
    }; 
};
// On renvoie les données vers l'application
echo json_encode($dataDeleteGame);
exit();
?>

