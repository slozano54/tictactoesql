<?php
//=====================================================================================================================
// GESTION DE L'AFFICHAGE D'UNE GRID DE JEU DANS SON ETAT ENREGISTRÉ
//
// @author Sébastien LOZANO
//=====================================================================================================================

// On inclut le fichier d'outils
include_once('connectTools.php');
// On inclut le fichier qui contient nom_de_serveur, nom_bdd, login et password d'accès à la bdd mysql
include_once("connect.php");

// On définit les headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: *');
header('Content-type: application/json; charset=UTF-8');

// On récupère les données POST si il y en a dans un objet JSON que l'on transforme en objet PHP
$dataGame = json_decode(file_get_contents('php://input'));
// On ajoute quelques éléments au JSON pour le retour vers l'application
$dataGame->msgGameKO = '';
$dataGame->msgGameOK = '';
$dataGame->gameOK = false;
$dataGame->symbolGrid = 'pasDeSymbolDefini';

// On vérifie que des données sont bien soumises par le client
if (isset($dataGame)){
    // On récupère l'id unique de la partie
    $idGame = $dataGame->idGame;
    // On vérifie que cet identifiant n'est pas vide
    if (issetNotempty($idGame)) {
        // On ouvre une connexion au serveur MySQL 
        $connexion = mysqli_connect (SERVEUR, LOGIN, MDP);
        if (!$connexion) {
            $dataGame->msgGameKO .= 'pb de connexion';
            $dataGame->gameOK = false;
        } else {
            // On selectionne la base de données pour les requêtes
            mysqli_select_db ($connexion,BDD); 
            // On récupère les données de la partie idGame
            // On parcourt la bdd et on range les éventuelles parties identiques existants dans un tableau
            // On définit la requête
            $sql = 'SELECT count(*) FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$idGame).'"';            
            // Si la requete aboutie on traite sinon message d'erreur
            if ($req = mysqli_query($connexion,$sql)) {
                $dataGame->msgGameOK .= "SQL OK !<br>";
                // On récupère les résultats dans un tableau
                $dataPartie = mysqli_fetch_array($req);
                // On libère la mémoire
                mysqli_free_result($req);	
                if ($dataPartie[0]==1) {// Une seule partie trouvée
                    // On définit la requête
                    $sql = 'SELECT etat_partie,joueur_X,joueur_O,joueur_actif FROM diu_parties WHERE nom_partie="'.mysqli_escape_string($connexion,$idGame).'"';
                    // Si la requête aboutie on traite sinon message d'erreur
                    if ($req = mysqli_query($connexion,$sql)) {
                        // On récupère les résultats dans un tableau
                        $etatPartie = mysqli_fetch_array($req);
                        $dataGame->playerX = $etatPartie['joueur_X'];
                        $dataGame->playerO = $etatPartie['joueur_O'];
                        // On récupère nom et prénom des joueurs
                        $sqlNomPrenomX = 'SELECT nom,prenom FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$etatPartie['joueur_X']).'"';
                        if ($reqNomPrenomX = mysqli_query($connexion,$sqlNomPrenomX)) {
                            $nomPrenomX = mysqli_fetch_array($reqNomPrenomX);
                            $dataGame->nomX = $nomPrenomX['nom'];
                            $dataGame->prenomX = $nomPrenomX['prenom'];
                        };
                        // On libère la mémoire 
                        mysqli_free_result($reqNomPrenomX);
                        $sqlNomPrenomO = 'SELECT nom,prenom FROM diu_membres WHERE login="'.mysqli_escape_string($connexion,$etatPartie['joueur_O']).'"';
                        if ($reqNomPrenomO = mysqli_query($connexion,$sqlNomPrenomO)) {
                            $nomPrenomO = mysqli_fetch_array($reqNomPrenomO);
                            $dataGame->nomO = $nomPrenomO['nom'];
                            $dataGame->prenomO = $nomPrenomO['prenom'];
                        };
                        // On libère la mémoire 
                        mysqli_free_result($reqNomPrenomO);
                        $dataGame->currentState = $etatPartie['etat_partie'];
                        if ($etatPartie['joueur_X'] == $etatPartie['joueur_actif']) {
                            $dataGame->symbolGrid = 'cross';                            
                            $dataGame->activePlayer = 'X';
                        } else {
                            $dataGame->symbolGrid = 'circle';
                            $dataGame->activePlayer = 'O';
                        };                        
                        // On libère la mémoire
                        mysqli_free_result($req);
                    } else {
                        $dataGame->msgGameKO .= "Pb avec la recup de l'état de la partie. <br>";
                    }                   
                } else { // 0 ou plusieurs parties trouvées
                    $dataGame->msgGameKO .= "Pb avec ce nom de partie ! <br>";
                };
            } else {
                $dataGame->msgGameKO .= 'SQL KO!<br>'.$sql.'<br>'.mysqli_error();
            };
        };
        // On coupe la connexion à la BDD
		mysqli_close($connexion);
    };
};
// On renvoie les données vers l'application
echo json_encode($dataGame);
exit();
?>