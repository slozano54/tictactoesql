<!-- 
    Source pour l'écriture d'un REAMDE en mardown : https://www.makeareadme.com/ 
-->


<!-- ![Work in progress](http://www.repostatus.org/badges/latest/wip.svg) -->
![Project Status: Inactive – The project has reached a stable, usable state but is no longer being actively developed; support/maintenance will be provided as time allows.](https://www.repostatus.org/badges/latest/inactive.svg)

<!-- _The project has reached a stable, usable state but is no longer being actively developed;_

_Support/maintenance will be provided as time allows._ -->

<style type="text/css">
</style>
<!-- PROJECT LOGO -->
<p text-align="center">
  <a href="https://framagit.org/slozano54/tictactoesql">
    <img src="img/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h1 text-align="center">DIU EIL 2020/2022</h1>
  <h2 text-align="center">BLOC4 - HTML/CSS/JS/PHP/SQL - TIC TAC TOE - </h2>
  <h3 text-align="center">Auteur - Sébastien LOZANO</h3>

  <p text-align="center">
    <a href="https://framagit.org/slozano54/tictactoesql">Explore the docs</a>
    .    
    <a href="https://framagit.org/slozano54/tictactoesql/issues">Report Bug</a>    
    .
    <a href="https://framagit.org/slozano54/tictactoesql/issues">Request Feature</a>
  </p>
</p>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a> 
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>    
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project

> J'ai choisi de mener ce projet sous forme de **Single Page Application** via l'**API fetch**.
> 
>L'**API Fetch** permet en effet d'échanger des données avec le serveur qui va les traiter, ici avec PHP, et les renvoyer à l'application sans qu'il soit nécessaire de recharger la page.
> 
>Côté client la forme **Single Page Application** permet, en manipulant le DOM, de n'avoir qu'un seul fichier index.php qui sera dynamiquement modifié pour afficher les différentes "pages" souhaitées. 

Le projet consiste à créer un espace de jeu de Tic Tac Toe (morpion) sur le web. Les joueurs devront pouvoir s’inscrire, se connecter, lancer une partie, jouer (en tour par tour), et consulter les données (parties, membres, statistiques, etc.) 

L’espace doit être créé en PHP avec les fonctionnalités/pages attendues suivantes :
 - Page d’accueil
 - Page de connexion
 - Page de déconnexion
 - Page d’inscription
    - Un utilisateur sera a minima décrit par son login, son mot de passe, son nom, son prénom, et un avatar choisi parmi un ensemble d’images proposées.
    - L’avatar choisi (un nom, correspond à un fichier image sur le serveur) sera une information de la base de données.
 - Page de gestion de compte
    - A minima, le joueur peut changer de mot de passe et d’avatar, ce qui demandera de modifier le contenu de la base de données.
    - Optionnel : on peut penser à ajouter la possibilité de télécharger son propre avatar sous la forme d’une image qui sera alors stockée sur le serveur.
- Page de l’utilisateur connecté
    - Nom, prénom
    - Parties en cours (un lien cliquable pour accéder à chaque partie)
- Une page affichant l‘état d’une partie et permettant de jouer le prochain coup si c’est le tour de l’utilisateur
    -  Pour modéliser les parties, vous avez deux possibilités :
        -  Un fichier texte qui contient une ligne par partie, de type :
            - Nom_partie | joueur_X | joueur_O | etat_partie | qui_le_tour
                - **etat_partie** est une chaine de caractères de longueur 9 contenant des "X" , des "O" et des "_" .
                - **qui_le_tour** vaut "X" ou "O" selon à qui c’est le tour

        >Cette modélisation ne permet pas d’enregistrer l’ordre des coups. Si vous voulez manipuler cette information, vous pouvez proposer une autre modélisation.

        - Des tables dans une base de données SQL

        > Comme ce n’est pas ce qui est demandé dans une première idée, je vous laisse le soin de réfléchir à un tel système d’information.

- Une page permettant de créer une partie. Il faudra permettre d’inviter un adversaire (qui ne pourra refuser, par simplicité), puisé dans la liste des inscrits proposée par la page. Une partie devra être décrite par un nom (exemple : ma_premiere_partie, kingkong_vs_godzilla, barbie_vs_ken, catane_partie_1, etc.) afin de la différencier des autres. Attention à refuser les noms déjà utilisés.
- Une page affichant les membres, avec des statistiques (nombre de parties gagnées, perdues,
en cours)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Disposer : 
- d'un serveur local supportant PHP ou d'un hebergement web.
- d'une base de données

### Installation

Cloner [le dépot](https://framagit.org/slozano54/tictactoesql)

```shell
git clone https://framagit.org/slozano54/tictactoesql
```

Ou télécharger l'[archive zip](https://framagit.org/slozano54/tictactoesql/-/archive/master/tictactoesql-master.zip).

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://framagit.org/slozano54/tictactoesql/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

[MIT](https://choosealicense.com/licenses/mit/)

<!-- CONTACT -->
## Contact

Sébastien LOZANO - Write me on gitlab

Project Link: [https://framagit.org/slozano54/tictactoesql](https://framagit.org/slozano54/tictactoesql)