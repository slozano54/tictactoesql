#!/bin/bash

mkdir compiled -p
#The compiled folder holds all the separate compiled CSS files.

subtypes=(general hero cards doc buttons grid headings icons forms navbar tables messages tictactoeGrid modalBox)
#You can change the subtypes used by changing this.

rm -f ../tictactoe.min.css
rm -f ../tictactoe.css
#Remove the old files.

i="0"

echo -n "/* Copyright 2021 Sébastien LOZANO; MIT licensed */" >> ../tictactoe.min.css
echo "/* Copyright 2021 Sébastien LOZANO; MIT licensed */" >> ../tictactoe.css
#Begin making tictactoe.min.css; -n is to remove the newline

#For each subtype, we compile the LESS file, minify it, and concatenate it into two files:
for item in ${subtypes[*]}
do
lessc "less/"$item".less" > "compiled/"$item".css"
#Compile all the LESS files.

java -jar yuicompressor-2.4.8.jar "compiled/"$item".css" -o "compiled/"$item".min.css"
#Compress all the compiled CSS files.

cat "compiled/"$item".min.css" >> ../tictactoe.min.css
#Write the compiled and minified CSS to tictactoe.min.css

cat "compiled/"$item".css" >> ../tictactoe.css
#Write the compiled and minified CSS to tictactoe.css

i=$i+1
done