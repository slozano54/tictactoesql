Gestion du css global
===

### Compiler une version minifiée

ATTENTION ! Pour modifier le css, ne pas bidouiller à la main dans les fichiers css mais : 
- éditer et modifier les fichiers less du dossier **css/src-less-css/less**

> Par exemple le fichier **css/src-less-css/less/buttons.less** contient les règles ccs à compiler pour les boutons !

- puis les compiler.

Il faut installer [lessc](https://lesscss.org/), pour compiler les fichiers less en fichiers css

- Installer globalement le module node  : less
> Necéssite l'installation de nodejs !
```bash
    pnpm install -g less
```
- Compiler le fichier styles.less en fichier styles.css
```bash
    lessc styles.less styles.css
```

> Java doit être installé pour lancer YUI compressor.

- Modifier les fichiers du dossier **less** puis lancer la commande :
```bash
    bash build.sh
```
Plusieurs fichiers sont générés dont **tictactoe.css** et **tictactoe.min.css** directement dans le dossier **css**

### License
MIT licensed.
