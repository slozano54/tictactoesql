<?php
// On démarre une nouvelle session ou on reprend une session existante
session_start();

// On inclut le fichier d'outils
include_once ('./php/siteTools.php');

// On inclut le fichier d'outils de connexion
include_once ('./espaceMembres/connectTools.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo $nomSiteOngletNav ?></title>
  		<meta name="title" content="<?php echo $nomSiteOngletNav ?>">
  		<meta name="description" content="<?php echo($metaDescription); ?>">
		
		<!-- Favicons -->
		<link href="./img/favico.ico" rel="icon">
  		<link href="./img/logo.png" rel="apple-touch-icon"> 
  		<!-- apple-touch-icon devrait etre en 114x114 -->
		<link href="./css/tictactoe.min.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<!-- Navigation pricipale -->
		<nav class="nav" tabindex="-1" onclick="this.focus()">
			<div class="container">				
				<a class="pagename current" href="#" id="pageName">TicTacToe</a>
				<a href="#" id="loginInfos" class="current"></a>
				<a href="#" id="showConnexion">Connexion</a>				
				<a href="./espaceMembres/deconnexion.php" id="deconnexion">Deconnexion</a>				
				<a href="#" id="showInscription">Inscription</a>
				<a href="#" id="showDocumentation">Documentation</a>
				<a href="#" id="statsNav">Statistiques</a>
				<a href="./docCss.html" target="_blank">Doc CSS</a>				
			</div>
		</nav>		
		<!-- container principal  -->
		<div class="container">
			<!-- div principale, visible même hors mode connecté -->
			<div class="hero">
				<div class="row">
					<div class="col c8">
						<h1>Amazing TicTacToe</h1>
						<!-- Message d'accueil -->
						<div id="msgAccueil" class="marges"></div>
						<!-- Pour les divers affichages en mode connecté -->
						<div id='sessionOKProfil' class="marges"></div>	
						<div id="sessionOKProfilFeedbacks"></div>
						<!-- Pour afficher le formulaire de création d'une partie -->
						<div id="createGame" class="marges" style="display:none;">
							<?php echo '<h3>Up '.$_SESSION['prenom'].' '.$_SESSION['nom'].',</h3>';?>
							<h4>Compléter le formulaire suivant pour inviter un adversaire dans une nouvelle partie.</h4>
							<form method="post" id="formCreateGame">
								<select name="opponentCreateGame" id="opponentCreateGame">
									<option value="Choisir">--Choisir un adversaire--</option>
									<?php 
										$vsList = vsList($_SESSION['login']);										
										foreach ($vsList as $vsPlayer) {
											echo '
											<option value="'.$vsPlayer->login.'">'.$vsPlayer->firstLastName.'</option>
											';		
										};										
									?>
								</select>								
								<br>
								<span class="addon">✍</span><input type="text" class="smooth" placeholder="Nom de la partie" name="nameCreateGame" id="nameCreateGame" value=""><br>
								<div class="msg"><strong>🕱 Attention 🕱</strong> Le nom de la partie ne doit contenir que des lettres.
								</div>
								<input class="btn btn-b btn-sm" type="submit" name="createGame" value="Valider la création de la partie" id="submitCreateGame">
							</form>
							<message id="msgCreateGameKO" class="warning"></message>
							<message id="msgCreateGameOK" class="great"></message>
						</div>
						<!-- Pour afficher la documentation -->
						<div id="documentation" style="display:none;">													
							<div class="msg">
								<strong>Documentation</strong>
								- Cliquer sur les titres pour montrer/cacher les infos.
							</div>
							<h4 id="btnDocInscriptionConnexion" class="btnDoc ">I - Inscription / Connexion</h4>
							<p id="docInscriptionConnexion">
								<img src="./img/documentation/inscription.png" class="docImg" loading="lazy"/>							
								<img src="./img/documentation/connexion.png" class="docImg" loading="lazy"/>							
							</p>							
							<h4 id="btnDocConnectedMode" class="btnDoc">II - Mode connecté</h4>
							<div id="docConnectedMode">
								<ul>
									<li id="btnDocAccueil" class="btnDoc"><h5>Accueil - Infos compte</h5></li>
									<p id="docAccueil">
										<img src="./img/documentation/accueil.png" class="docImg" loading="lazy"/>
										<img src="./img/documentation/infosCompte.png" class="docImg" loading="lazy"/>							
									</p>
									<li id="btnDocModifProfil" class="btnDoc"><h5>Modification du compte</h5></li>
									<p id="docModifProfil">
										<img src="./img/documentation/modification.png" class="docImg" loading="lazy"/>							
									</p>
									<li id="btnDocShowStats" class="btnDoc"><h5>Affichage des statistiques</h5></li>
									<p id="docShowStats">
										<img src="./img/documentation/statistiques.png" class="docImg" loading="lazy"/>							
									</p>
									<li id="btnDocShowGames" class="btnDoc"><h5>Voir ses parties</h5></li>
									<p id="docShowGames">
										<img src="./img/documentation/voirMesParties.png" class="docImg" loading="lazy"/>							
									</p>
									<li id="btnDocCreateGame" class="btnDoc"><h5>Créer une partie</h5></li>						
									<p id="docCreateGame">
										<img src="./img/documentation/creerUnePartie.png" class="docImg" loading="lazy"/>								
									</p>
								</ul>
							</div>
						</div>
						<!-- Pour afficher une grille de jeu -->
						<div id="gameZone" style="display:none;">
							<section id="grid" data-idgame="" class="">
								<div data-index="0" data-value="_" class="case"></div>
								<div data-index="1" data-value="_" class="case"></div>
								<div data-index="2" data-value="_" class="case"></div>
								<div data-index="3" data-value="_" class="case"></div>
								<div data-index="4" data-value="_" class="case"></div>
								<div data-index="5" data-value="_" class="case"></div>
								<div data-index="6" data-value="_" class="case"></div>
								<div data-index="7" data-value="_" class="case"></div>
								<div data-index="8" data-value="_" class="case"></div>
							</section>
							<div class="msg" id="msgGameZone">
								<strong>FeedBack Game Zone</strong>
								<p id="feedbackMsgGameZone" data-currentplayer="" data-winner=""></p>    
							</div>
						</div>
						<!-- Pour afficher une boite de confirmation personnalisée -->						
						<div class="modal">
							<div class="modalContent">
								<span class="closeButton">&times;</span>
								<p id="modalBody"></p>
								<div id="modalFeedbacks"></div>
								<br>
								<a href="#" class="btn btn-b btn-sm" id="modalAbortDelete">Annuler</a>
								<a href="#" class="btn btn-c btn-sm" id="modalValidDelete">Valider</a>
							</div>
						</div>
					</div>					
					<div class="col c4">
						<!-- Pour afficher le formulaire de connexion -->
						<div class="col c12" id="connexion" style="display:none;">
							<h4>Connexion</h4>							
							<form method="post" id="formConnexion">
								<span class="addon">♟</span><input type="text" class="smooth" placeholder="Nom d'utilisateur" id="loginConnexion" name="loginConnexion" value=""><br>								
								<span class="addon">🔒</span><input type="password" class="smooth" placeholder="Mot de passe"id="pwdConnexion" name="pwdConnexion" value=""><br>
								<input class="btn btn-b btn-sm" type="submit" name="connexion" value="Connexion" id="submitConnexion">
							</form>
							<message id="msgConnexionKO" class="warning"></message>
							<message id="msgConnexionOK" class="great"></message>
						</div>
						<!-- Pour afficher le formulaire d'inscription -->
						<div class="col c12" id="inscription" style="display:none;">
							<h4>Inscription</h4>							
							<form method="post" id="formInscription">
								<span class="addon">♟</span><input type="text" class="smooth" placeholder="Nom d'utilisateur" name="loginInscription" id="loginInscription" value=""><br>								
								<span class="addon">🔒</span><input type="password" class="smooth" placeholder="Mot de passe" id="pwdInscription1" name="pwdInscription1" value=""><br>
								<span class="addon">🔒</span><input type="password" class="smooth" placeholder="Confirmer mot de passe" id="pwdInscription2" name="pwdInscription2" value=""><br>
								<span class="addon">✍</span><input type="text" class="smooth" placeholder="Nom" name="nomInscription" id="nomInscription" value=""><br>
								<span class="addon">✍</span><input type="text" class="smooth" placeholder="Prénom" name="prenomInscription" id="prenomInscription" value=""><br>
								<div>
									<!-- On affiche 3 avatars du dossier ./img/avatars au hasard -->
									<?php
										$i=1;
										foreach (filesAlea("./img/avatars/") as $avatar) {
											echo '
											<input type="radio" id="avatarInscription'.$i.'" name="avatarInscription" value="'.$avatar.'"';											
											if ($i == 1) {
												echo 'checked="checked"';
											};
											echo '>
											<label for="avatarInscription'.$i.'"><img src="'.$avatar.'"/></label>
											';		
											$i++;
										}
									?>
								</div>
								<input class="btn btn-b btn-sm" type="submit" name="inscription" value="Inscription" id="submitInscription">
							</form>
							<message id="msgInscriptionKO" class="warning"></message>
							<message id="msgInscriptionOK" class="great"></message>
							<message class="msg">Tu pourras changer ton avatar dans ton espace personnel après ton inscription.</message>
						</div>
						<!-- Pour afficher le formulaire de modification du compte -->
						<div class="col c12" id="modification" style="display:none;">
							<h4>Modification du compte</h4>							
							<message class="msg"><strong>Note : </strong> Le login ne peut pas être modifié.</message><br>							
							<form method="post" id="formModification">
								<span class="addon">♟</span><input type="text" class="smooth" placeholder="Nom d'utilisateur" name="loginModification" id="loginModification" value="" disabled="disabled"><br>								
								<span class="addon">🔒</span><input type="password" class="smooth" placeholder="Mot de passe" id="pwdModification1" name="pwdModification1" value=""><br>
								<span class="addon">🔒</span><input type="password" class="smooth" placeholder="Confirmation du mot de passe" id="pwdModification2" name="pwdModification2" value=""><br>
								<span class="addon">✍</span><input type="text" class="smooth" placeholder="Nom" name="nomModification" id="nomModification" value=""><br>
								<span class="addon">✍</span><input type="text" class="smooth" placeholder="Prénom" name="prenomModification" id="prenomModification" value=""><br>
								<div id="avatars">
									<!-- On affiche 3 avatars du dossier ./img/avatars au hasard -->									
									<input type="radio" id="avatarModification1" name="avatarModification" value="" checked="checked">
									<label for="avatarModification1"><img id="avatarModificationImg" src=""/></label>
									<input type="radio" id="avatarModification2" name="avatarModification" value="" >
									<label for="avatarModification2"><img id="avatarModificationImg2" src=""/></label>
									<input type="radio" id="avatarModification3" name="avatarModification" value="" >
									<label for="avatarModification3"><img id="avatarModificationImg3" src=""/></label>
								</div>
								<div id="avatarPerso">
									<message class="msg"><strong>Avatar perso :</strong>
									<ul>
										<li>Fichier jpg ou png.</li>
										<li>Moins de 2 Mo.</li>										
									</ul>									
									</message><br>																
									<input type="file" id="avatarPersoInputFile" name="avatarPersoInputFile">
								</div>
								<input class="btn btn-b btn-sm" type="submit" name="modification" value="Modification" id="submitModification">
								<input class="btn btn-b btn-sm" type="button" name="modificationAbandon" value="Abandon" id="modificationAbandon">
							</form>
							<message id="msgModificationKO" class="warning"></message>
							<message id="msgModificationOK" class="great"></message>
						</div>
					</div>
				</div>
			</div>
			<!-- div d'actions, visible uniquement en mode connecté -->
			<div class="row" id="sessionOKStuffs" style="display:none;">
				<div class="col c4 card">
					<h3>Gestion du compte</h3>
					Modifier son compte en proposant éventuellement un avatar perso.<br>
					<a href="#" class="btn btn-a btn-sm" id="showModification">Modifier le compte</a>
					<a href="#" class="btn btn-a btn-sm" id="showModificationAvatarPerso">Modifier le compte - Avatar perso</a>
				</div>
				<div class="col c4 card">
					<h3>Gestion des parties</h3>
					Pour voir toutes les parties du joueur ou créer une nouvelle partie.<br>
					<a href="#" class="btn btn-b btn-sm" id="myGames">Voir mes parties</a>
					<a href="#" class="btn btn-b btn-sm" id="newGame">Créer une partie</a>
				</div>
				<div class="col c4 card">
					<h3>Statistiques</h3>
					Affichage des membres avec des statistiques, nombre de parties gagnées, perdues, nulles, en cours.<br>
					<a href="#" class="btn btn-c btn-sm" id="statsStuffs">Voir les stats</a>
				</div>
			</div>		
		</div>  <!-- ./container -->
		<footer>
			<p>
        		DIU INFO 2020/2022 - &copy; Sébastien LOZANO <br> <?php echo $anneeCopyright ?> - TIC TAC TOE version <?php echo $verLoz ?>
			</p>
    	</footer>
		<!-- Script js principal de l'application -->
		<script type="module" src="js/main.js"> </script>
	</body>
</html>